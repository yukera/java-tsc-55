package com.tsc.jarinchekhina.tm.api.entity;

public interface IHasNameMethod {

  String name();

}
