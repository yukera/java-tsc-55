package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.endpoint.TaskDTO;
import com.tsc.jarinchekhina.tm.endpoint.TaskEndpoint;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

  @NotNull
  @Autowired
  protected TaskEndpoint taskEndpoint;

  public void print(@NotNull final TaskDTO task) {
    System.out.println("ID: " + task.getId());
    System.out.println("NAME: " + task.getName());
    System.out.println("DESCRIPTION: " + task.getDescription());
    System.out.println("STATUS: " + task.getStatus().value());
    System.out.println("PROJECT ID: " + task.getProjectId());
  }

}
