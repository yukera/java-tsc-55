package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.endpoint.UserDTO;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Getter
@Component
public abstract class AbstractUserCommand extends AbstractCommand {


  public void print(@NotNull final UserDTO user) {
    System.out.println("LOGIN: " + user.getLogin());
    System.out.println("E-MAIL: " + user.getEmail());
    System.out.println("FIRST NAME: " + user.getFirstName());
    System.out.println("LAST NAME: " + user.getLastName());
    System.out.println("MIDDLE NAME: " + user.getMiddleName());
  }

}
