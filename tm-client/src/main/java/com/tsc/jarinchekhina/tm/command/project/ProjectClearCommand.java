package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "project-clear";
  }

  @NotNull
  @Override
  public String description() {
    return "remove all projects";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[CLEAR PROJECTS]");
    getProjectEndpoint().clearProjects(serviceLocator.getSession());
  }

}
