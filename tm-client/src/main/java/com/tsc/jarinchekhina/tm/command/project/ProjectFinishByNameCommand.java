package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ProjectFinishByNameCommand extends AbstractProjectCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "project-finish-by-name";
  }

  @NotNull
  @Override
  public String description() {
    return "finish project by name";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[FINISH PROJECT]");
    System.out.println("ENTER NAME:");
    @NotNull final String name = TerminalUtil.nextLine();
    getProjectEndpoint().finishProjectByName(serviceLocator.getSession(), name);
  }

}
