package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "project-remove-by-id";
  }

  @NotNull
  @Override
  public String description() {
    return "remove project by id";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[REMOVE PROJECT]");
    System.out.println("ENTER ID:");
    @NotNull final String id = TerminalUtil.nextLine();
    getProjectEndpoint().removeProjectById(serviceLocator.getSession(), id);
  }

}
