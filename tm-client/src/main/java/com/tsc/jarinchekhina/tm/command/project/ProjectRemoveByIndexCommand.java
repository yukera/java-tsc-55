package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.endpoint.ProjectDTO;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "project-remove-by-index";
  }

  @NotNull
  @Override
  public String description() {
    return "remove project by index";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[REMOVE PROJECT]");
    System.out.println("ENTER INDEX:");
    @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
    @NotNull final ProjectDTO projectByIndex = getProjectEndpoint().findProjectByIndex(serviceLocator.getSession(), index);
    if (projectByIndex == null) throw new ProjectNotFoundException();
    getProjectEndpoint().removeProjectById(serviceLocator.getSession(), projectByIndex.getId());
  }

}
