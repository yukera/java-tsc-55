package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ExitCommand extends AbstractCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "system-exit";
  }

  @NotNull
  @Override
  public String description() {
    return "close application";
  }

  @Override
  public void execute() {
    System.exit(0);
  }

}
