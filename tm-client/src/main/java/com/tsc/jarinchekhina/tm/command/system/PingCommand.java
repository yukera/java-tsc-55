package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpoint;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class PingCommand extends AbstractUserCommand {

  @NotNull
  @Autowired
  private SessionEndpoint sessionEndpoint;

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "system-ping";
  }

  @NotNull
  @Override
  public String description() {
    return "ping server";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[PING]");
    if (sessionEndpoint.ping())
      System.out.println("Success");
    else
      System.out.println("No connect");
  }

}
