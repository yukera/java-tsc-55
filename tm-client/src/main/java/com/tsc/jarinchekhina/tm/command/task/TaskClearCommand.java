package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-clear";
  }

  @NotNull
  @Override
  public String description() {
    return "remove all tasks";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[CLEAR TASKS]");
    getTaskEndpoint().clearTasks(serviceLocator.getSession());
  }

}
