package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-create";
  }

  @NotNull
  @Override
  public String description() {
    return "create new task";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[CREATE TASK]");
    System.out.println("ENTER NAME:");
    @NotNull final String name = TerminalUtil.nextLine();
    System.out.println("ENTER DESCRIPTION:");
    @NotNull final String description = TerminalUtil.nextLine();
    getTaskEndpoint().createTaskWithDescription(serviceLocator.getSession(), name, description);
  }

}
