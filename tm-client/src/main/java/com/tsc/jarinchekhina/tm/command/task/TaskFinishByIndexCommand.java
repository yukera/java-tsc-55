package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskFinishByIndexCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-finish-by-index";
  }

  @NotNull
  @Override
  public String description() {
    return "finish task by index";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[FINISH TASK]");
    System.out.println("ENTER INDEX:");
    @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
    getTaskEndpoint().finishTaskByIndex(serviceLocator.getSession(), index);
  }

}
