package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskFinishByNameCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-finish-by-name";
  }

  @NotNull
  @Override
  public String description() {
    return "finish task by name";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[FINISH TASK]");
    System.out.println("ENTER NAME:");
    @NotNull final String name = TerminalUtil.nextLine();
    getTaskEndpoint().finishTaskByName(serviceLocator.getSession(), name);
  }

}
