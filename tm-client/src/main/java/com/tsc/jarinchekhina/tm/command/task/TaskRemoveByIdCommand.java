package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-remove-by-id";
  }

  @NotNull
  @Override
  public String description() {
    return "remove task by id";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[REMOVE TASK]");
    System.out.println("ENTER ID:");
    @NotNull final String id = TerminalUtil.nextLine();
    getTaskEndpoint().removeTaskById(serviceLocator.getSession(), id);
  }

}
