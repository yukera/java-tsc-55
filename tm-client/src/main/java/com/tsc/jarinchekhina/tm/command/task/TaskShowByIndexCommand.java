package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.endpoint.TaskDTO;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskShowByIndexCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-show-by-index";
  }

  @NotNull
  @Override
  public String description() {
    return "show task by index";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[SHOW TASK]");
    System.out.println("ENTER INDEX:");
    @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
    @NotNull final TaskDTO task = getTaskEndpoint().findTaskByIndex(serviceLocator.getSession(), index);
    if (task == null) throw new TaskNotFoundException();
    print(task);
  }

}
