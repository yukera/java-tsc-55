package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.endpoint.TaskDTO;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-show-by-project-id";
  }

  @NotNull
  @Override
  public String description() {
    return "show tasks by project id";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[LIST TASKS BY PROJECT ID]");
    System.out.println("ENTER PROJECT ID:");
    @NotNull final String id = TerminalUtil.nextLine();
    @NotNull final List<TaskDTO> tasks = getTaskEndpoint().findAllTaskByProjectId(serviceLocator.getSession(), id);
    int index = 1;
    for (@NotNull final TaskDTO task : tasks) {
      System.out.println(index + ". " + task);
      index++;
    }
  }

}