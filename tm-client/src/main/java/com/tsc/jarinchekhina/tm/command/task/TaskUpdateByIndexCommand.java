package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "task-update-by-index";
  }

  @NotNull
  @Override
  public String description() {
    return "update task by index";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[UPDATE TASK]");
    System.out.println("ENTER INDEX:");
    @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
    System.out.println("ENTER NAME:");
    @NotNull final String name = TerminalUtil.nextLine();
    System.out.println("ENTER DESCRIPTION:");
    @NotNull final String description = TerminalUtil.nextLine();
    getTaskEndpoint().updateTaskByIndex(serviceLocator.getSession(), index, name, description);
  }

}
