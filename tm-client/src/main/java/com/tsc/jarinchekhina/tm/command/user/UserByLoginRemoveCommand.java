package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.endpoint.AdminUserEndpoint;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserByLoginRemoveCommand extends AbstractUserCommand {

  @NotNull
  @Autowired
  private AdminUserEndpoint adminUserEndpoint;

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "user-remove-by-login";
  }

  @NotNull
  @Override
  public String description() {
    return "remove user by login";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[REMOVE USER]");
    System.out.println("ENTER LOGIN:");
    @NotNull final String login = TerminalUtil.nextLine();
    adminUserEndpoint.removeByLogin(serviceLocator.getSession(), login);
  }

}
