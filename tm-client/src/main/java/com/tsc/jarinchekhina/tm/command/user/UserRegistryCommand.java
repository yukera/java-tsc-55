package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.endpoint.UserEndpoint;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserRegistryCommand extends AbstractUserCommand {

  @NotNull
  @Autowired
  private UserEndpoint userEndpoint;

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "registry";
  }

  @NotNull
  @Override
  public String description() {
    return "registry to system";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[REGISTRY]");
    System.out.println("ENTER LOGIN:");
    @NotNull final String login = TerminalUtil.nextLine();
    System.out.println("ENTER E-MAIL:");
    @NotNull final String email = TerminalUtil.nextLine();
    System.out.println("ENTER PASSWORD:");
    @NotNull final String password = TerminalUtil.nextLine();
    userEndpoint.createUserWithEmail(login, password, email);
  }

}
