package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.endpoint.UserEndpoint;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserUpdateProfileCommand extends AbstractUserCommand {

  @NotNull
  @Autowired
  private UserEndpoint userEndpoint;

  @Nullable
  @Override
  public String arg() {
    return null;
  }

  @NotNull
  @Override
  public String name() {
    return "user-update";
  }

  @NotNull
  @Override
  public String description() {
    return "update user profile";
  }

  @SneakyThrows
  @Override
  public void execute() {
    System.out.println("[UPDATE PROFILE]");
    System.out.println("ENTER FIRST NAME:");
    @NotNull final String firstName = TerminalUtil.nextLine();
    System.out.println("ENTER LAST NAME:");
    @NotNull final String lastName = TerminalUtil.nextLine();
    System.out.println("ENTER MIDDLE NAME:");
    @NotNull final String middleName = TerminalUtil.nextLine();
    userEndpoint.updateUser(serviceLocator.getSession(), firstName, lastName, middleName);
  }

}
