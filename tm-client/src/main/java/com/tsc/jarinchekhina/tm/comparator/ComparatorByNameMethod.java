package com.tsc.jarinchekhina.tm.comparator;

import com.tsc.jarinchekhina.tm.api.entity.IHasNameMethod;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByNameMethod implements Comparator<IHasNameMethod> {

  @NotNull
  private static final ComparatorByNameMethod INSTANCE = new ComparatorByNameMethod();

  @NotNull
  public static ComparatorByNameMethod getInstance() {
    return INSTANCE;
  }

  @Override
  public int compare(@Nullable final IHasNameMethod o1, @Nullable final IHasNameMethod o2) {
    if (o1 == null || o2 == null) return 0;
    return o1.name().compareTo(o2.name());
  }

}
