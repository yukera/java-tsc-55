package com.tsc.jarinchekhina.tm.config;

import com.tsc.jarinchekhina.tm.endpoint.AdminEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.AdminEndpointService;
import com.tsc.jarinchekhina.tm.endpoint.AdminUserEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.AdminUserEndpointService;
import com.tsc.jarinchekhina.tm.endpoint.ProjectEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.ProjectEndpointService;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpointService;
import com.tsc.jarinchekhina.tm.endpoint.TaskEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.TaskEndpointService;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * ClientConfig
 *
 * @author Yuliya Arinchekhina
 */
@Configuration
@ComponentScan("com.tsc.jarinchekhina.tm")
public class ClientConfig {

  @Bean
  @NotNull
  public AdminEndpoint adminEndpoint() {
    @NotNull final AdminEndpointService adminEndpointService = new AdminEndpointService();
    return adminEndpointService.getAdminEndpointPort();
  }

  @Bean
  @NotNull
  public AdminUserEndpoint adminUserEndpoint() {
    @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    return adminUserEndpointService.getAdminUserEndpointPort();
  }

  @Bean
  @NotNull
  public SessionEndpoint sessionEndpoint() {
    @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    return sessionEndpointService.getSessionEndpointPort();
  }

  @Bean
  @NotNull
  public ProjectEndpoint projectEndpoint() {
    @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    return projectEndpointService.getProjectEndpointPort();
  }

  @Bean
  @NotNull
  public TaskEndpoint taskEndpoint() {
    @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
    return taskEndpointService.getTaskEndpointPort();
  }

}
