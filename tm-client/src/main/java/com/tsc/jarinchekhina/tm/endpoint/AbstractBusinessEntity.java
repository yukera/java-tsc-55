package com.tsc.jarinchekhina.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for abstractBusinessEntity complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="abstractBusinessEntity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.tm.jarinchekhina.tsc.com/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="user" type="{http://endpoint.tm.jarinchekhina.tsc.com/}user" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abstractBusinessEntity", propOrder = {
    "user"
})
@XmlSeeAlso({
    Session.class,
    Project.class,
    Task.class
})
public abstract class AbstractBusinessEntity
    extends AbstractEntity {

  protected User user;

  /**
   * Gets the value of the user property.
   *
   * @return possible object is
   * {@link User }
   */
  public User getUser() {
    return user;
  }

  /**
   * Sets the value of the user property.
   *
   * @param value allowed object is
   *              {@link User }
   */
  public void setUser(User value) {
    this.user = value;
  }

}
