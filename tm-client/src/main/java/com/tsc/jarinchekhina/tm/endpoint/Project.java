package com.tsc.jarinchekhina.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for project complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="project"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.tm.jarinchekhina.tsc.com/}abstractBusinessEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dateFinish" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dateStart" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://endpoint.tm.jarinchekhina.tsc.com/}status" minOccurs="0"/&gt;
 *         &lt;element name="tasks" type="{http://endpoint.tm.jarinchekhina.tsc.com/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "project", propOrder = {
    "created",
    "dateFinish",
    "dateStart",
    "description",
    "name",
    "status",
    "tasks"
})
public class Project
    extends AbstractBusinessEntity {

  @XmlSchemaType(name = "dateTime")
  protected XMLGregorianCalendar created;
  @XmlSchemaType(name = "dateTime")
  protected XMLGregorianCalendar dateFinish;
  @XmlSchemaType(name = "dateTime")
  protected XMLGregorianCalendar dateStart;
  protected String description;
  protected String name;
  @XmlSchemaType(name = "string")
  protected Status status;
  @XmlElement(nillable = true)
  protected List<Task> tasks;

  /**
   * Gets the value of the created property.
   *
   * @return possible object is
   * {@link XMLGregorianCalendar }
   */
  public XMLGregorianCalendar getCreated() {
    return created;
  }

  /**
   * Sets the value of the created property.
   *
   * @param value allowed object is
   *              {@link XMLGregorianCalendar }
   */
  public void setCreated(XMLGregorianCalendar value) {
    this.created = value;
  }

  /**
   * Gets the value of the dateFinish property.
   *
   * @return possible object is
   * {@link XMLGregorianCalendar }
   */
  public XMLGregorianCalendar getDateFinish() {
    return dateFinish;
  }

  /**
   * Sets the value of the dateFinish property.
   *
   * @param value allowed object is
   *              {@link XMLGregorianCalendar }
   */
  public void setDateFinish(XMLGregorianCalendar value) {
    this.dateFinish = value;
  }

  /**
   * Gets the value of the dateStart property.
   *
   * @return possible object is
   * {@link XMLGregorianCalendar }
   */
  public XMLGregorianCalendar getDateStart() {
    return dateStart;
  }

  /**
   * Sets the value of the dateStart property.
   *
   * @param value allowed object is
   *              {@link XMLGregorianCalendar }
   */
  public void setDateStart(XMLGregorianCalendar value) {
    this.dateStart = value;
  }

  /**
   * Gets the value of the description property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the value of the description property.
   *
   * @param value allowed object is
   *              {@link String }
   */
  public void setDescription(String value) {
    this.description = value;
  }

  /**
   * Gets the value of the name property.
   *
   * @return possible object is
   * {@link String }
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param value allowed object is
   *              {@link String }
   */
  public void setName(String value) {
    this.name = value;
  }

  /**
   * Gets the value of the status property.
   *
   * @return possible object is
   * {@link Status }
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Sets the value of the status property.
   *
   * @param value allowed object is
   *              {@link Status }
   */
  public void setStatus(Status value) {
    this.status = value;
  }

  /**
   * Gets the value of the tasks property.
   *
   * <p>
   * This accessor method returns a reference to the live list,
   * not a snapshot. Therefore any modification you make to the
   * returned list will be present inside the JAXB object.
   * This is why there is not a <CODE>set</CODE> method for the tasks property.
   *
   * <p>
   * For example, to add a new item, do as follows:
   * <pre>
   *    getTasks().add(newItem);
   * </pre>
   *
   *
   * <p>
   * Objects of the following type(s) are allowed in the list
   * {@link Task }
   */
  public List<Task> getTasks() {
    if (tasks == null) {
      tasks = new ArrayList<Task>();
    }
    return this.tasks;
  }

}
