package com.tsc.jarinchekhina.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public abstract class AbstractException extends Exception {

  public AbstractException(@NotNull Throwable cause) {
    super(cause);
  }

  public AbstractException(@NotNull final String error) {
    super(error);
  }

}
