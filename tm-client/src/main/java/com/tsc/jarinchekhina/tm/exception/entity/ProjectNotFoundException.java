package com.tsc.jarinchekhina.tm.exception.entity;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

  public ProjectNotFoundException() {
    super("Error! Project not found...");
  }

}
