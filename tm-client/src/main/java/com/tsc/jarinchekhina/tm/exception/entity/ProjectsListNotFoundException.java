package com.tsc.jarinchekhina.tm.exception.entity;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class ProjectsListNotFoundException extends AbstractException {

  public ProjectsListNotFoundException() {
    super("Error! Projects not found...");
  }

}
