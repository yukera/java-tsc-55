package com.tsc.jarinchekhina.tm.exception.entity;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class TasksListNotFoundException extends AbstractException {

  public TasksListNotFoundException() {
    super("Error! Tasks not found...");
  }

}
