package com.tsc.jarinchekhina.tm.exception.system;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UnknownCommandException extends AbstractException {

  public UnknownCommandException(@NotNull final String command) {
    super("Error! Command '" + command + "' not found... Try 'system-help' to get list of available commands.");
  }

}
