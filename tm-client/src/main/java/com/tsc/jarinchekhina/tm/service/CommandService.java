package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.service.ICommandService;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public final class CommandService implements ICommandService {

  @NotNull
  @Autowired
  private final ICommandRepository commandRepository;

  @NotNull
  @Override
  public List<AbstractCommand> getCommandList() {
    return commandRepository.getCommandList();
  }

  @NotNull
  @Override
  public Collection<AbstractCommand> getCommands() {
    return commandRepository.getCommands();
  }

  @NotNull
  @Override
  public Collection<AbstractCommand> getArguments() {
    return commandRepository.getArguments();
  }

  @NotNull
  @Override
  public Collection<String> getCommandNames() {
    return commandRepository.getCommandNames();
  }

  @NotNull
  @Override
  public Collection<String> getCommandArgs() {
    return commandRepository.getCommandArgs();
  }

  @Nullable
  @Override
  public AbstractCommand getCommandByName(@Nullable final String name) {
    if (DataUtil.isEmpty(name)) return null;
    return commandRepository.getCommandByName(name);
  }

  @Nullable
  @Override
  public AbstractCommand getCommandByArg(@Nullable final String name) {
    if (DataUtil.isEmpty(name)) return null;
    return commandRepository.getCommandByArg(name);
  }

  @Override
  public void add(@Nullable final AbstractCommand command) {
    if (command == null) return;
    commandRepository.add(command);
  }

}
