package com.tsc.jarinchekhina.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@UtilityClass
public class DataUtil {

  public static boolean isEmpty(@Nullable final String value) {
    return (value == null || value.isEmpty());
  }

  public static boolean isEmpty(@Nullable final Collection value) {
    return (value == null || value.isEmpty());
  }

  public static boolean isEmpty(@Nullable final String[] values) {
    return (values == null || values.length == 0);
  }

  public static boolean isEmpty(@Nullable final Integer value) {
    return (value == null || value < 0);
  }

}
