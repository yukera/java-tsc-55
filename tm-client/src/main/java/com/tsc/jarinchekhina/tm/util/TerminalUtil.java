package com.tsc.jarinchekhina.tm.util;

import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

@UtilityClass
public class TerminalUtil {

  @NotNull
  private static final Scanner SCANNER = new Scanner(System.in);

  @NotNull
  @SneakyThrows
  public Integer nextNumber() {
    @NotNull final String value = nextLine();
    try {
      return Integer.parseInt(value);
    } catch (Exception e) {
      throw new IndexIncorrectException(value);
    }
  }

  @NotNull
  public String nextLine() {
    return SCANNER.nextLine();
  }

}
