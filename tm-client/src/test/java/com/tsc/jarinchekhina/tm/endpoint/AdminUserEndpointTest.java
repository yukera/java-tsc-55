package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class AdminUserEndpointTest {

  @NotNull
  private static final Bootstrap bootstrap = new Bootstrap();

  @Nullable
  private static SessionDTO adminSession;

  @Before
  public void before() {
    adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
    bootstrap.getUserEndpoint().createUserWithEmail("autotest", "autotest", "autotest@tsc.tu");
  }

  @After
  public void after() {
    bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
    bootstrap.getSessionEndpoint().closeSession(adminSession);
  }

  @Test
  @Category(IntegrationCategory.class)
  public void testFindByLogin() {
    @Nullable UserDTO user = bootstrap.getAdminUserEndpoint().findByLogin(adminSession, "autotest");
    Assert.assertNotNull(user);
  }

  @Test(expected = Exception.class)
  @Category(IntegrationCategory.class)
  public void testLockByLogin() {
    bootstrap.getAdminUserEndpoint().lockByLogin(adminSession, "autotest");
    bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
  }

  @Test
  @Category(IntegrationCategory.class)
  public void testUnlockByLogin() {
    bootstrap.getAdminUserEndpoint().unlockByLogin(adminSession, "autotest");
    @Nullable SessionDTO userSession = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
    Assert.assertNotNull(userSession);
  }

}
