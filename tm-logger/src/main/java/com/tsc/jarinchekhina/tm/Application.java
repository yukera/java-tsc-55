package com.tsc.jarinchekhina.tm;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.config.LoggerConfig;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

  public static void main(final String[] args) {
    @NotNull AnnotationConfigApplicationContext context =
        new AnnotationConfigApplicationContext(LoggerConfig.class);
    final Bootstrap bootstrap = context.getBean(Bootstrap.class);
    bootstrap.init();
  }

}