package com.tsc.jarinchekhina.tm.api;

import org.jetbrains.annotations.NotNull;

/**
 * ILoggerService
 *
 * @author Yuliya Arinchekhina
 */
public interface ILoggerService {

  void log(@NotNull String jsonEntity);

}
