package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.listener.LoggerListener;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;

@Component
public final class Bootstrap {

  @NotNull
  @Autowired
  private ConnectionFactory connectionFactory;

  @SneakyThrows
  public void init() {
    @NotNull final Connection connection = connectionFactory.createConnection();
    connection.start();
    @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    @NotNull final Destination destination = session.createQueue("TM_QUEUE");
    final MessageConsumer consumer = session.createConsumer(destination);
    consumer.setMessageListener(new LoggerListener());
  }

}
