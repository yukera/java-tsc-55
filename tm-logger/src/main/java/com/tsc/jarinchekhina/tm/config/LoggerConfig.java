package com.tsc.jarinchekhina.tm.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.jms.ConnectionFactory;

/**
 * LoggerConfig
 *
 * @author Yuliya Arinchekhina
 */
@ComponentScan("com.tsc.jarinchekhina.tm")
public class LoggerConfig {

  @Bean
  public ConnectionFactory connectionFactory() {
    @NotNull final ActiveMQConnectionFactory connectionFactory =
        new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
    connectionFactory.setTrustAllPackages(true);
    return connectionFactory;
  }

}
