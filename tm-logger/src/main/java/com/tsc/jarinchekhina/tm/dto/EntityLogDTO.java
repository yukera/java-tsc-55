package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * EntityLogDTO
 *
 * @author Yuliya Arinchekhina
 */
@Getter
@Setter
public class EntityLogDTO {

  private final String id = UUID.randomUUID().toString();
  private String formattedDate;
  private String className;
  private EntityActionType actionType;
  private Object entityDTO;
  private String tableName;

  public EntityLogDTO() {
  }

}
