package com.tsc.jarinchekhina.tm.enumerated;

/**
 * EntityAction
 *
 * @author Yuliya Arinchekhina
 */
public enum EntityActionType {

  PRE_PERSIST,
  POST_PERSIST,
  PRE_REMOVE,
  POST_REMOVE,
  PRE_UPDATE,
  POST_UPDATE

}
