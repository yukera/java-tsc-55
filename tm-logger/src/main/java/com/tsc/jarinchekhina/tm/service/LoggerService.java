package com.tsc.jarinchekhina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.tsc.jarinchekhina.tm.api.ILoggerService;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

/**
 * LoggerService
 *
 * @author Yuliya Arinchekhina
 */
@Service
public class LoggerService implements ILoggerService {

  @NotNull
  private final ObjectMapper objectMapper = new ObjectMapper();

  @NotNull
  private final MongoClient mongoClient = new MongoClient("localhost", 27017);

  @NotNull
  private final MongoDatabase mongoDatabase = mongoClient.getDatabase("tm_logger");

  @Override
  @SneakyThrows
  public void log(@NotNull final String jsonEntity) {
    @NotNull final LinkedHashMap<String, Object> message = objectMapper.readValue(jsonEntity, LinkedHashMap.class);
    @NotNull final String collectionName = message.get("tableName").toString();
    if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
    @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
    collection.insertOne(new Document(message));
  }

}
