package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

  @NotNull
  List<E> findAll();

  @NotNull
  E findById(@NotNull String id);

  void add(@NotNull E entity);

  void addAll(@NotNull Collection<E> collection);

  void update(@NotNull E entity);

  void clear();

  void removeById(@NotNull String id);

  void remove(@NotNull E entity);

}
