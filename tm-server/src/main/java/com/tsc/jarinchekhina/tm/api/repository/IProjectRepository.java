package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

  void add(@NotNull String userId, @NotNull String name);

  void add(@NotNull String userId, @NotNull Project project);

  void clear(@NotNull String userId);

  void remove(@NotNull String userId, @NotNull Project project);

  void removeById(@NotNull String userId, @NotNull String id);

  void removeByName(@NotNull String userId, @NotNull String name);

  @NotNull
  List<Project> findAll(@NotNull String userId);

  @Nullable
  Project findById(@NotNull String userId, @NotNull String id);

  @Nullable
  Project findByIndex(@NotNull String userId, @NotNull Integer index);

  @Nullable
  Project findByName(@NotNull String userId, @NotNull String name);

}