package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

  void clear(@NotNull String userId);

  void removeAllByProjectId(@NotNull String projectId);

  void remove(@NotNull String userId, @NotNull Task task);

  void removeById(@NotNull String userId, @NotNull String id);

  void removeByName(@NotNull String userId, @NotNull String name);

  @NotNull
  List<Task> findAll(@NotNull String userId);

  @NotNull
  List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

  @Nullable
  Task findById(@NotNull String userId, @NotNull String id);

  @Nullable
  Task findByIndex(@NotNull String userId, @NotNull Integer index);

  @Nullable
  Task findByName(@NotNull String userId, @NotNull String name);

}
