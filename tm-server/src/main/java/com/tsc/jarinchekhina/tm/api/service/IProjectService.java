package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<Project> {

  void add(@Nullable String userId, @Nullable Project project);

  void create(@Nullable String userId, @Nullable String name);

  void create(@Nullable String userId, @Nullable String name, @Nullable String description);

  void clear(@Nullable String userId);

  void remove(@Nullable String userId, @Nullable Project project);

  void removeById(@Nullable String userId, @Nullable String id);

  void removeByIndex(@Nullable String userId, @Nullable Integer index);

  void removeByName(@Nullable String userId, @Nullable String name);

  @NotNull
  List<Project> findAll(@Nullable String userId);

  @NotNull
  Project findById(@Nullable String userId, @Nullable String id);

  @NotNull
  Project findByIndex(@Nullable String userId, @Nullable Integer index);

  @NotNull
  Project findByName(@Nullable String userId, @Nullable String name);

  void updateProjectById(
      @Nullable String userId,
      @Nullable String id,
      @Nullable String name,
      @Nullable String description
  );

  void updateProjectByIndex(
      @Nullable String userId,
      @Nullable Integer index,
      @Nullable String name,
      @Nullable String description
  );

  void startProjectById(@Nullable String userId, @Nullable String id);

  void startProjectByIndex(@Nullable String userId, @Nullable Integer index);

  void startProjectByName(@Nullable String userId, @Nullable String name);

  void finishProjectById(@Nullable String userId, @Nullable String id);

  void finishProjectByIndex(@Nullable String userId, @Nullable Integer index);

  void finishProjectByName(@Nullable String userId, @Nullable String name);

  void changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

  void changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

  void changeProjectStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);


}
