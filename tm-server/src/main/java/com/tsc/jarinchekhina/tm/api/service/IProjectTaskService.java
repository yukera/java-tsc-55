package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

  @NotNull
  Task bindTaskByProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

  void clearProjects(@Nullable String userId);

  @NotNull
  List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

  void removeProjectById(@Nullable String userId, @Nullable String projectId);

  @NotNull
  Task unbindTaskByProjectId(@Nullable String userId, @Nullable String taskId);

}
