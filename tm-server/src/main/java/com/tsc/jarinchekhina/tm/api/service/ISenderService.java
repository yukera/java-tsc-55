package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.dto.EntityLogDTO;
import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import org.jetbrains.annotations.NotNull;

/**
 * ISenderService
 *
 * @author Yuliya Arinchekhina
 */
public interface ISenderService {

  EntityLogDTO createMessage(@NotNull Object object, @NotNull EntityActionType actionType);

  void send(@NotNull EntityLogDTO entity);

}
