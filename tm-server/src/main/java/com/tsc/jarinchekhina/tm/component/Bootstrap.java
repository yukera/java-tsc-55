package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.endpoint.IAdminUserEndpoint;
import com.tsc.jarinchekhina.tm.api.endpoint.IProjectEndpoint;
import com.tsc.jarinchekhina.tm.api.endpoint.ISessionEndpoint;
import com.tsc.jarinchekhina.tm.api.endpoint.ITaskEndpoint;
import com.tsc.jarinchekhina.tm.api.endpoint.IUserEndpoint;
import com.tsc.jarinchekhina.tm.api.repository.IConnectionProvider;
import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.api.service.ISessionService;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.endpoint.AdminUserEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.ProjectEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.TaskEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.UserEndpoint;
import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.service.ConnectionService;
import com.tsc.jarinchekhina.tm.service.ProjectService;
import com.tsc.jarinchekhina.tm.service.ProjectTaskService;
import com.tsc.jarinchekhina.tm.service.PropertyService;
import com.tsc.jarinchekhina.tm.service.SessionService;
import com.tsc.jarinchekhina.tm.service.TaskService;
import com.tsc.jarinchekhina.tm.service.UserService;
import com.tsc.jarinchekhina.tm.util.SystemUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements IServiceLocator, IConnectionProvider {

  @NotNull
  private static final MessageExecutor messageExecutor = new MessageExecutor();

  @NotNull
  private final IPropertyService propertyService = new PropertyService();

  @NotNull
  private final IConnectionService connectionService = new ConnectionService(this);

  @NotNull
  private final IProjectService projectService = new ProjectService(connectionService);

  @NotNull
  private final ITaskService taskService = new TaskService(connectionService);

  @NotNull
  private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

  @NotNull
  private final IUserService userService = new UserService(connectionService, propertyService);

  @NotNull
  private final ISessionService sessionService = new SessionService(connectionService, propertyService, userService);

  @NotNull
  private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

  @NotNull
  private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

  @NotNull
  private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

  @NotNull
  private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

  @NotNull
  private final IUserEndpoint userEndpoint = new UserEndpoint(this);

  public static void sendMessage(
      @NotNull final Object object,
      @NotNull final EntityActionType actionType
  ) {
    messageExecutor.sendMessage(object, actionType);
  }

  @SneakyThrows
  public void init() {
    System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
    initUsers();
    initPID();
    initEndpoints();
  }

  public void initUsers() {
    if (getUserService().findByLogin("admin") == null)
      getUserService().create("admin", "admin", Role.ADMIN);
    if (getUserService().findByLogin("test") == null)
      getUserService().create("test", "test", "test@test.ru");
  }

  private void initEndpoints() {
    registry(userEndpoint);
    registry(sessionEndpoint);
    registry(taskEndpoint);
    registry(projectEndpoint);
    registry(adminUserEndpoint);
  }

  private void registry(@Nullable final Object endpoint) {
    if (endpoint == null) return;
    @NotNull final String host = propertyService.getServerHost();
    @NotNull final Integer port = propertyService.getServerPort();
    @NotNull final String name = endpoint.getClass().getSimpleName();
    @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
    System.out.println(wsdl);
    Endpoint.publish(wsdl, endpoint);
  }

  @SneakyThrows
  private void initPID() {
    @NotNull final String fileName = "task-manager-server.pid";
    @NotNull final String pid = Long.toString(SystemUtil.getPID());
    Files.write(Paths.get(fileName), pid.getBytes());
    final File file = new File(fileName);
    file.deleteOnExit();
  }

}