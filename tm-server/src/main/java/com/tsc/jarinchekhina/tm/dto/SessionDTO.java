package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.listener.EntityDTOListener;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@EntityListeners(EntityDTOListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionDTO extends AbstractEntityDTO implements Cloneable {

  @Column
  @Nullable
  private Long timestamp;
  @Column(name = "user_id")
  @Nullable
  private String userId;
  @Column
  @Nullable
  private String signature;

  public SessionDTO(@Nullable final Session session) {
    this.setId(session.getId());
    @Nullable final User user = session.getUser();
    if (user != null) this.userId = user.getId();
    this.timestamp = session.getTimestamp();
    this.signature = session.getSignature();
  }

  @Nullable
  @Override
  public SessionDTO clone() {
    try {
      return (SessionDTO) super.clone();
    } catch (CloneNotSupportedException e) {
      return null;
    }
  }

}
