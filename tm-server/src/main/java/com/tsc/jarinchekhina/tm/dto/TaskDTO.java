package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.api.entity.IWBS;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.listener.EntityDTOListener;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_task")
@EntityListeners(EntityDTOListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskDTO extends AbstractEntityDTO implements IWBS {

  @Column(name = "user_id")
  @NotNull
  private String userId;
  @Column
  @NotNull
  private String name;
  @Column
  @Nullable
  private String description;
  @Column
  @NotNull
  @Enumerated(EnumType.STRING)
  private Status status = Status.NOT_STARTED;
  @Column(name = "project_id")
  @Nullable
  private String projectId;
  @Column(name = "date_start")
  @Nullable
  private Date dateStart;
  @Column(name = "date_finish")
  @Nullable
  private Date dateFinish;
  @Column
  @Nullable
  private Date created = new Date();

  public TaskDTO(@Nullable final Task task) {
    this.setId(task.getId());
    @Nullable final User user = task.getUser();
    if (user != null) this.userId = user.getId();
    @Nullable final Project project = task.getProject();
    if (project != null) this.projectId = project.getId();
    this.name = task.getName();
    this.description = task.getDescription();
    this.status = task.getStatus();
    this.dateStart = task.getDateStart();
    this.dateFinish = task.getDateFinish();
    this.created = task.getCreated();
  }

  @Override
  public String toString() {
    return getId() + ": " + getName();
  }

}
