package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.listener.EntityDTOListener;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_user")
@EntityListeners(EntityDTOListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDTO extends AbstractEntityDTO {

  @Column
  @NotNull
  private String login;
  @Column(name = "password_hash")
  @NotNull
  private String passwordHash;
  @Column
  @Nullable
  private String email;
  @Column(name = "first_name")
  @Nullable
  private String firstName;
  @Column(name = "last_name")
  @Nullable
  private String lastName;
  @Column(name = "middle_name")
  @Nullable
  private String middleName;
  @Column
  @NotNull
  @Enumerated(EnumType.STRING)
  private Role role = Role.USER;
  private boolean locked = false;

  public UserDTO(@Nullable final User user) {
    this.setId(user.getId());
    this.login = user.getLogin();
    this.passwordHash = user.getPasswordHash();
    this.email = user.getEmail();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.middleName = user.getMiddleName();
    this.role = user.getRole();
    this.locked = user.isLocked();
  }

}
