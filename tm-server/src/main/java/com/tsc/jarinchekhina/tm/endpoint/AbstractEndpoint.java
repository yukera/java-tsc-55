package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractEndpoint {

  @NotNull
  protected IServiceLocator serviceLocator;

  public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  public AbstractEndpoint() {
    throw new RuntimeException();
  }

}
