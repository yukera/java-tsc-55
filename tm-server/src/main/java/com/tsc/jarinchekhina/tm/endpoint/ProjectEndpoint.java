package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IProjectEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

  public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
    super(serviceLocator);
  }

  @Override
  @WebMethod
  public void changeProjectStatusById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id,
      @WebParam(name = "status", partName = "status") @Nullable final Status status
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().changeProjectStatusById(session.getUserId(), id, status);
  }

  @Override
  @WebMethod
  public void changeProjectStatusByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index,
      @WebParam(name = "status", partName = "status") @Nullable final Status status
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().changeProjectStatusByIndex(session.getUserId(), index, status);
  }

  @Override
  @WebMethod
  public void changeProjectStatusByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "status", partName = "status") @Nullable final Status status
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().changeProjectStatusByName(session.getUserId(), name, status);
  }

  @Override
  @WebMethod
  public void clearProjects(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().clear(session.getUserId());
  }

  @Override
  @WebMethod
  public void createProject(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().create(session.getUserId(), name);
  }

  @Override
  @WebMethod
  public void createProjectWithDescription(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "description", partName = "description") @Nullable final String description
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().create(session.getUserId(), name, description);
  }

  @NotNull
  @Override
  @WebMethod
  public List<ProjectDTO> findAllProjects(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Project.toDTO(serviceLocator.getProjectService().findAll(session.getUserId()));
  }

  @NotNull
  @Override
  @WebMethod
  public ProjectDTO findProjectById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Project.toDTO(serviceLocator.getProjectService().findById(session.getUserId(), id));
  }

  @NotNull
  @Override
  @WebMethod
  public ProjectDTO findProjectByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Project.toDTO(serviceLocator.getProjectService().findByIndex(session.getUserId(), index));
  }

  @NotNull
  @Override
  @WebMethod
  public ProjectDTO findProjectByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Project.toDTO(serviceLocator.getProjectService().findByName(session.getUserId(), name));
  }

  @Override
  @WebMethod
  public void finishProjectById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().finishProjectById(session.getUserId(), id);
  }

  @Override
  @WebMethod
  public void finishProjectByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().finishProjectByIndex(session.getUserId(), index);
  }

  @Override
  @WebMethod
  public void finishProjectByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().finishProjectByName(session.getUserId(), name);
  }

  @Override
  @WebMethod
  public void removeProjectById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), id);
  }

  @Override
  @WebMethod
  public void removeProjectByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    @NotNull Project project = serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), project.getId());
  }

  @Override
  @WebMethod
  public void removeProjectByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    @NotNull Project project = serviceLocator.getProjectService().findByName(session.getUserId(), name);
    serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), project.getId());
  }

  @Override
  @WebMethod
  public void startProjectById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().startProjectById(session.getUserId(), id);
  }

  @Override
  @WebMethod
  public void startProjectByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().startProjectByIndex(session.getUserId(), index);
  }

  @Override
  @WebMethod
  public void startProjectByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().startProjectByName(session.getUserId(), name);
  }

  @Override
  @WebMethod
  public void updateProjectById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "description", partName = "description") @Nullable final String description
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
  }

  @Override
  @WebMethod
  public void updateProjectByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "description", partName = "description") @Nullable final String description
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
  }

}
