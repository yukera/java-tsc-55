package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ITaskEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

  public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
    super(serviceLocator);
  }

  @NotNull
  @Override
  @WebMethod
  public TaskDTO bindTaskByProjectId(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
      @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Task.toDTO(serviceLocator.getProjectTaskService()
                                    .bindTaskByProjectId(session.getUserId(), projectId, taskId));
  }

  @Override
  @WebMethod
  public void changeTaskStatusById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id,
      @WebParam(name = "status", partName = "status") @Nullable final Status status
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().changeTaskStatusById(session.getUserId(), id, status);
  }

  @Override
  @WebMethod
  public void changeTaskStatusByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index,
      @WebParam(name = "status", partName = "status") @Nullable final Status status
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().changeTaskStatusByIndex(session.getUserId(), index, status);
  }

  @Override
  @WebMethod
  public void changeTaskStatusByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "status", partName = "status") @Nullable final Status status
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().changeTaskStatusByName(session.getUserId(), name, status);
  }

  @Override
  @WebMethod
  public void clearTasks(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().clear(session.getUserId());
  }

  @Override
  @WebMethod
  public void createTask(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().create(session.getUserId(), name);
  }

  @Override
  @WebMethod
  public void createTaskWithDescription(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "description", partName = "description") @Nullable final String description
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().create(session.getUserId(), name, description);
  }

  @NotNull
  @Override
  @WebMethod
  public List<TaskDTO> findAllTaskByProjectId(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Task.toDTO(serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUserId(), projectId));
  }

  @NotNull
  @Override
  @WebMethod
  public List<TaskDTO> findAllTasks(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Task.toDTO(serviceLocator.getTaskService().findAll(session.getUserId()));
  }

  @NotNull
  @Override
  @WebMethod
  public TaskDTO findTaskById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Task.toDTO(serviceLocator.getTaskService().findById(session.getUserId(), id));
  }

  @NotNull
  @Override
  @WebMethod
  public TaskDTO findTaskByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Task.toDTO(serviceLocator.getTaskService().findByIndex(session.getUserId(), index));
  }

  @NotNull
  @Override
  @WebMethod
  public TaskDTO findTaskByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Task.toDTO(serviceLocator.getTaskService().findByName(session.getUserId(), name));
  }

  @Override
  @WebMethod
  public void finishTaskById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().finishTaskById(session.getUserId(), id);
  }

  @Override
  @WebMethod
  public void finishTaskByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().finishTaskByIndex(session.getUserId(), index);
  }

  @Override
  @WebMethod
  public void finishTaskByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().finishTaskByName(session.getUserId(), name);
  }

  @Override
  @WebMethod
  public void removeTaskById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().removeById(session.getUserId(), id);
  }

  @Override
  @WebMethod
  public void removeTaskByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
  }

  @Override
  @WebMethod
  public void removeTaskByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().removeByName(session.getUserId(), name);
  }

  @Override
  @WebMethod
  public void startTaskById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().startTaskById(session.getUserId(), id);
  }

  @Override
  @WebMethod
  public void startTaskByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().startTaskByIndex(session.getUserId(), index);
  }

  @Override
  @WebMethod
  public void startTaskByName(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "name", partName = "name") @Nullable final String name
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().startTaskByName(session.getUserId(), name);
  }

  @NotNull
  @Override
  @WebMethod
  public TaskDTO unbindTaskByProjectId(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    return Task.toDTO(serviceLocator.getProjectTaskService().unbindTaskByProjectId(session.getUserId(), taskId));
  }

  @Override
  @WebMethod
  public void updateTaskById(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "id", partName = "id") @Nullable final String id,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "description", partName = "description") @Nullable final String description
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
  }

  @Override
  @WebMethod
  public void updateTaskByIndex(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "index", partName = "index") @Nullable final Integer index,
      @WebParam(name = "name", partName = "name") @Nullable final String name,
      @WebParam(name = "description", partName = "description") @Nullable final String description
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
    serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
  }

}
