package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IUserEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

  public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
    super(serviceLocator);
  }

  @NotNull
  @Override
  @WebMethod
  public UserDTO createUser(
      @WebParam(name = "login", partName = "login") @Nullable final String login,
      @WebParam(name = "password", partName = "password") @Nullable final String password
  ) {
    return User.toDTO(serviceLocator.getUserService().create(login, password));
  }

  @NotNull
  @Override
  @WebMethod
  public UserDTO createUserWithEmail(
      @WebParam(name = "login", partName = "login") @Nullable final String login,
      @WebParam(name = "password", partName = "password") @Nullable final String password,
      @WebParam(name = "email", partName = "email") @Nullable final String email
  ) {
    return User.toDTO(serviceLocator.getUserService().create(login, password, email));
  }

  @NotNull
  @Override
  @WebMethod
  public UserDTO createUserWithRole(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "login", partName = "login") @Nullable final String login,
      @WebParam(name = "password", partName = "password") @Nullable final String password,
      @WebParam(name = "role", partName = "role") @Nullable final Role role
  ) {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
    return User.toDTO(serviceLocator.getUserService().create(login, password, role));
  }

  @NotNull
  @Override
  @WebMethod
  public UserDTO setPassword(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "password", partName = "password") @Nullable final String password
  ) {
    serviceLocator.getSessionService().validate(session);
    return User.toDTO(serviceLocator.getUserService().setPassword(session.getUserId(), password));
  }

  @NotNull
  @Override
  @WebMethod
  public UserDTO updateUser(
      @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
      @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
      @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName,
      @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
  ) {
    serviceLocator.getSessionService().validate(session);
    return User.toDTO(serviceLocator.getUserService().update(session.getUserId(), firstName, lastName, middleName));
  }

}
