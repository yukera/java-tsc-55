package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyDescriptionException extends AbstractException {

  public EmptyDescriptionException() {
    super("Error! Description is empty...");
  }

}
