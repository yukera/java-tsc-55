package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

  public EmptyEmailException() {
    super("Error! E-mail is empty...");
  }

}
