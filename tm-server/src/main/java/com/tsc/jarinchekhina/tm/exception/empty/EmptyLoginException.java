package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

  public EmptyLoginException() {
    super("Error! Login is empty...");
  }

}
