package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

  public EmptyNameException() {
    super("Error! Name is empty...");
  }

}
