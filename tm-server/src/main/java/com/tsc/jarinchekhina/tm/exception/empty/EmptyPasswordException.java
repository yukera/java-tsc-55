package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

  public EmptyPasswordException() {
    super("Error! Password is empty...");
  }

}
