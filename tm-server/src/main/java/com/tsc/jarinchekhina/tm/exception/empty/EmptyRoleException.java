package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyRoleException extends AbstractException {

  public EmptyRoleException() {
    super("Error! Role is empty...");
  }

}
