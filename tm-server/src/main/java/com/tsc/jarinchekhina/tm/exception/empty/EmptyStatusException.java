package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyStatusException extends AbstractException {

  public EmptyStatusException() {
    super("Error! Status is empty...");
  }

}
