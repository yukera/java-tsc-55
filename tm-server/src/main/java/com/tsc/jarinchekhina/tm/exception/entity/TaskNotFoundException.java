package com.tsc.jarinchekhina.tm.exception.entity;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

  public TaskNotFoundException() {
    super("Error! Task not found...");
  }

}
