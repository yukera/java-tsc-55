package com.tsc.jarinchekhina.tm.exception.system;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public final class NoFileException extends AbstractException {

  public NoFileException(@NotNull final String filename) {
    super("Error! File" + filename + "doesn't exist...");
  }

}
