package com.tsc.jarinchekhina.tm.exception.user;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class EmailRegisteredException extends AbstractException {

  public EmailRegisteredException(@NotNull final String email) {
    super("Error! User with e-mail '" + email + "' is already registered...");
  }

}
