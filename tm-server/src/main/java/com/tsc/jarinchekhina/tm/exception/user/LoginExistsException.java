package com.tsc.jarinchekhina.tm.exception.user;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class LoginExistsException extends AbstractException {

  public LoginExistsException(@NotNull final String login) {
    super("Error! Login '" + login + "' already exists...");
  }

}
