package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.listener.EntityListener;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "tm_project")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Project extends AbstractBusinessEntity {

  @Column
  @NotNull
  private String name;

  @Column
  @Nullable
  private String description;

  @Column
  @NotNull
  @Enumerated(EnumType.STRING)
  private Status status = Status.NOT_STARTED;

  @Column(name = "date_start")
  @Nullable
  private Date dateStart;

  @Column(name = "date_finish")
  @Nullable
  private Date dateFinish;

  @Column
  @Nullable
  private Date created = new Date();

  @Nullable
  @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
  private transient List<Task> tasks = new ArrayList<>();

  @NotNull
  public static List<ProjectDTO> toDTO(@Nullable final List<Project> projectList) {
    if (DataUtil.isEmpty(projectList)) return Collections.emptyList();
    @NotNull final List<ProjectDTO> result = new ArrayList<>();
    for (@Nullable final Project project : projectList) {
      if (project == null) continue;
      result.add(Project.toDTO(project));
    }
    return result;
  }

  @Nullable
  public static ProjectDTO toDTO(@Nullable final Project project) {
    if (project == null) return null;
    return new ProjectDTO(project);
  }

}
