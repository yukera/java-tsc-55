package com.tsc.jarinchekhina.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.listener.EntityListener;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@EntityListeners(EntityListener.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractBusinessEntity implements Cloneable {

  @Column
  @Nullable
  private Long timestamp;

  @Column
  @Nullable
  private String signature;

  @Nullable
  public static SessionDTO toDTO(@Nullable final Session session) {
    if (session == null) return null;
    return new SessionDTO(session);
  }

  @Nullable
  @Override
  public Session clone() {
    try {
      return (Session) super.clone();
    } catch (CloneNotSupportedException e) {
      return null;
    }
  }

}
