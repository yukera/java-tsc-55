package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.listener.EntityListener;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "tm_user")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User extends AbstractEntity {

  @Column
  @NotNull
  private String login;

  @Column(name = "password_hash")
  @NotNull
  private String passwordHash;

  @Column
  @Nullable
  private String email;

  @Column(name = "first_name")
  @Nullable
  private String firstName;

  @Column(name = "last_name")
  @Nullable
  private String lastName;

  @Column(name = "middle_name")
  @Nullable
  private String middleName;

  @Column
  @NotNull
  @Enumerated(EnumType.STRING)
  private Role role = Role.USER;

  private boolean locked = false;

  @Nullable
  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private transient List<Session> sessions = new ArrayList<>();

  @Nullable
  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private transient List<Project> projects = new ArrayList<>();

  @Nullable
  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private transient List<Task> tasks = new ArrayList<>();

  @Nullable
  public static UserDTO toDTO(@Nullable final User user) {
    if (user == null) return null;
    return new UserDTO(user);
  }

}
