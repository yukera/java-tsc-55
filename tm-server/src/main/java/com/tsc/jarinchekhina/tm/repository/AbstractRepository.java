package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.model.AbstractEntity;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

  @NotNull
  private EntityManager entityManager;

  public AbstractRepository(@NotNull final EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  @NotNull
  @Override
  public abstract List<E> findAll();

  @Override
  public abstract void add(@NotNull final E entity);

  @NotNull
  @Override
  public abstract E findById(@NotNull final String id);

  @Override
  public abstract void clear();

  @Override
  public abstract void removeById(@NotNull final String id);

  @Override
  public abstract void remove(@NotNull final E entity);

}
