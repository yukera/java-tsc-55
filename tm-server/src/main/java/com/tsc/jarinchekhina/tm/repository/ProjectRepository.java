package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

  public ProjectRepository(@NotNull final EntityManager entityManager) {
    super(entityManager);
  }

  public void add(@NotNull final Project project) {
    getEntityManager().persist(project);
  }

  public void add(@NotNull final String userId, @NotNull final String name) {
    @NotNull final User user = getEntityManager().find(User.class, userId);
    @NotNull final Project project = new Project();
    project.setName(name);
    project.setUser(user);
    getEntityManager().persist(project);
  }

  @Override
  public void add(@NotNull final String userId, @NotNull final Project project) {
    @NotNull final User user = getEntityManager().find(User.class, userId);
    project.setUser(user);
    getEntityManager().persist(project);
  }

  @Override
  public void addAll(@NotNull final Collection<Project> collection) {
    for (@NotNull Project project : collection) {
      getEntityManager().persist(project);
    }
  }

  public void update(@NotNull final Project project) {
    getEntityManager().merge(project);
  }

  @Override
  @SneakyThrows
  public void clear() {
    getEntityManager()
        .createQuery("DELETE FROM Project")
        .executeUpdate();
  }

  @Override
  @SneakyThrows
  public void clear(@NotNull final String userId) {
    getEntityManager()
        .createQuery("DELETE FROM Project e WHERE e.user.id = :userId")
        .setParameter("userId", userId)
        .executeUpdate();
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<Project> findAll() {
    return getEntityManager()
        .createQuery("SELECT e FROM Project e", Project.class)
        .getResultList();
  }

  @NotNull
  @Override
  @SneakyThrows
  public Project findByIndex(@NotNull final String userId, @NotNull final Integer index) {
    @NotNull final List<Project> projectList = findAll(userId);
    if (projectList.isEmpty()) throw new ProjectNotFoundException();
    return projectList.get(index - 1);
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<Project> findAll(@NotNull final String userId) {
    return getEntityManager()
        .createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
        .setParameter("userId", userId)
        .getResultList();
  }

  @Override
  @SneakyThrows
  public void remove(@NotNull final Project project) {
    getEntityManager().remove(project);
  }

  @Override
  @SneakyThrows
  public void remove(@NotNull final String userId, @NotNull final Project project) {
    if (!userId.equals(project.getUser().getId())) throw new AccessDeniedException();
    getEntityManager().remove(project);
  }

  @Override
  @SneakyThrows
  public void removeById(@NotNull final String id) {
    @NotNull final Project project = getEntityManager().getReference(Project.class, id);
    getEntityManager().remove(project);
  }

  @Override
  @SneakyThrows
  public void removeById(@NotNull final String userId, @NotNull final String id) {
    @NotNull final Project project = findById(userId, id);
    getEntityManager().remove(project);
  }

  @NotNull
  @Override
  @SneakyThrows
  public Project findById(@NotNull final String userId, @NotNull final String id) {
    @NotNull final Project project = findById(id);
    checkUserId(userId, project);
    return project;
  }

  @NotNull
  @Override
  @SneakyThrows
  public Project findById(@NotNull final String id) {
    @Nullable final List<Project> projectList = getEntityManager()
        .createQuery("SELECT e FROM Project e WHERE e.id = :id", Project.class)
        .setParameter("id", id)
        .setMaxResults(1)
        .getResultList();
    if (projectList == null || projectList.isEmpty()) throw new ProjectNotFoundException();
    return projectList.get(0);
  }

  @SneakyThrows
  private void checkUserId(@NotNull final String userId, @NotNull final Project project) {
    if (!userId.equals(project.getUser().getId())) throw new ProjectNotFoundException();
  }

  @Override
  @SneakyThrows
  public void removeByName(@NotNull final String userId, @NotNull final String name) {
    @NotNull final Project project = findByName(userId, name);
    getEntityManager().remove(project);
  }

  @NotNull
  @Override
  @SneakyThrows
  public Project findByName(@NotNull final String userId, @NotNull final String name) {
    @Nullable final List<Project> projectList = getEntityManager()
        .createQuery("SELECT e FROM Project e WHERE e.name = :name", Project.class)
        .setParameter("name", name)
        .setMaxResults(1)
        .getResultList();
    if (projectList == null || projectList.isEmpty()) throw new ProjectNotFoundException();
    @NotNull final Project project = projectList.get(0);
    checkUserId(userId, project);
    return project;
  }

}
