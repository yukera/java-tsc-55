package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

  public TaskRepository(@NotNull final EntityManager entityManager) {
    super(entityManager);
  }

  public void add(@NotNull final Task task) {
    getEntityManager().persist(task);
  }

  public void add(@NotNull final String userId, @NotNull final Task task) {
    @NotNull final User user = getEntityManager().find(User.class, userId);
    task.setUser(user);
    getEntityManager().persist(task);
  }

  @Override
  public void addAll(@NotNull final Collection<Task> collection) {
    for (@NotNull Task task : collection) {
      getEntityManager().persist(task);
    }
  }

  public void update(@NotNull final Task task) {
    getEntityManager().merge(task);
  }

  @Override
  @SneakyThrows
  public void clear() {
    getEntityManager()
        .createQuery("DELETE FROM Task")
        .executeUpdate();
  }

  @Override
  @SneakyThrows
  public void clear(@NotNull final String userId) {
    getEntityManager()
        .createQuery("DELETE FROM Task e where e.user.id = :userId")
        .setParameter("userId", userId)
        .executeUpdate();
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<Task> findAll() {
    return getEntityManager()
        .createQuery("SELECT e FROM Task e", Task.class)
        .getResultList();
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
    return getEntityManager()
        .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId", Task.class)
        .setParameter("userId", userId)
        .setParameter("projectId", projectId)
        .getResultList();
  }

  @NotNull
  @Override
  @SneakyThrows
  public Task findByIndex(@NotNull final String userId, @NotNull final Integer index) {
    @NotNull final List<Task> taskList = findAll(userId);
    if (taskList.isEmpty()) throw new TaskNotFoundException();
    return taskList.get(index - 1);
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<Task> findAll(@NotNull final String userId) {
    return getEntityManager()
        .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
        .setParameter("userId", userId)
        .getResultList();
  }

  @Override
  @SneakyThrows
  public void removeAllByProjectId(@NotNull final String projectId) {
    getEntityManager()
        .createQuery("DELETE FROM Task e WHERE e.project.id = :projectId")
        .setParameter("projectId", projectId)
        .executeUpdate();
  }

  @Override
  @SneakyThrows
  public void remove(@NotNull final Task task) {
    getEntityManager().remove(task);
  }

  @Override
  @SneakyThrows
  public void remove(@NotNull final String userId, @NotNull final Task task) {
    if (!userId.equals(task.getUser().getId())) throw new AccessDeniedException();
    getEntityManager().remove(task);
  }

  @Override
  @SneakyThrows
  public void removeById(@NotNull final String id) {
    @NotNull final Task task = getEntityManager().getReference(Task.class, id);
    getEntityManager().remove(task);
  }

  @Override
  @SneakyThrows
  public void removeById(@NotNull final String userId, @NotNull final String id) {
    @NotNull final Task task = findById(userId, id);
    getEntityManager().remove(task);
  }

  @NotNull
  @Override
  @SneakyThrows
  public Task findById(@NotNull final String userId, @NotNull final String id) {
    @NotNull final Task task = findById(id);
    checkUserId(userId, task);
    return task;
  }

  @NotNull
  @Override
  @SneakyThrows
  public Task findById(@NotNull final String id) {
    @Nullable final List<Task> taskList = getEntityManager()
        .createQuery("SELECT e FROM Task e WHERE e.id = :id", Task.class)
        .setParameter("id", id)
        .setMaxResults(1)
        .getResultList();
    if (taskList == null || taskList.isEmpty()) throw new TaskNotFoundException();
    return taskList.get(0);
  }

  @SneakyThrows
  private void checkUserId(@NotNull final String userId, @NotNull final Task task) {
    if (!userId.equals(task.getUser().getId())) throw new TaskNotFoundException();
  }

  @Override
  @SneakyThrows
  public void removeByName(@NotNull final String userId, @NotNull final String name) {
    @NotNull final Task task = findByName(userId, name);
    getEntityManager().remove(task);
  }

  @NotNull
  @Override
  @SneakyThrows
  public Task findByName(@NotNull final String userId, @NotNull final String name) {
    @Nullable final List<Task> taskList = getEntityManager()
        .createQuery("SELECT e FROM Task e WHERE e.name = :name", Task.class)
        .setParameter("name", name)
        .setMaxResults(1)
        .getResultList();
    if (taskList.isEmpty()) throw new TaskNotFoundException();
    @NotNull final Task task = taskList.get(0);
    checkUserId(userId, task);
    return task;
  }

}
