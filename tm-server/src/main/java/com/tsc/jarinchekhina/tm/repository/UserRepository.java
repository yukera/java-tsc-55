package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

  public UserRepository(@NotNull final EntityManager entityManager) {
    super(entityManager);
  }

  public void add(@NotNull final User user) {
    getEntityManager().persist(user);
  }

  @Override
  public void addAll(@NotNull final Collection<User> collection) {
    for (@NotNull User user : collection) {
      getEntityManager().persist(user);
    }
  }

  public void update(@NotNull final User user) {
    getEntityManager().merge(user);
  }

  @Override
  @SneakyThrows
  public void clear() {
    getEntityManager()
        .createQuery("DELETE FROM User")
        .executeUpdate();
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<User> findAll() {
    return getEntityManager()
        .createQuery("SELECT e FROM User e", User.class)
        .getResultList();
  }

  @Nullable
  @Override
  @SneakyThrows
  public User findByEmail(@NotNull final String email) {
    @Nullable final List<User> userList = getEntityManager()
        .createQuery("SELECT e FROM User e WHERE e.email = :email", User.class)
        .setParameter("email", email)
        .setMaxResults(1)
        .getResultList();
    if (userList == null || userList.isEmpty()) return null;
    return userList.get(0);
  }

  @NotNull
  @Override
  @SneakyThrows
  public User findById(@NotNull final String id) {
    @Nullable final List<User> userList = getEntityManager()
        .createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
        .setParameter("id", id)
        .setMaxResults(1)
        .getResultList();
    if (userList == null || userList.isEmpty()) throw new UserNotFoundException();
    return userList.get(0);
  }

  @Override
  @SneakyThrows
  public void remove(@NotNull final User user) {
    getEntityManager().remove(user);
  }

  @Override
  @SneakyThrows
  public void removeById(@NotNull final String id) {
    @NotNull final User user = getEntityManager().getReference(User.class, id);
    getEntityManager().remove(user);
  }

  @Override
  @SneakyThrows
  public void removeByLogin(@NotNull final String login) {
    @Nullable final User user = findByLogin(login);
    if (user == null) throw new UserNotFoundException();
    getEntityManager().remove(user);
  }

  @Nullable
  @Override
  @SneakyThrows
  public User findByLogin(@NotNull final String login) {
    @Nullable final List<User> userList = getEntityManager()
        .createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
        .setParameter("login", login)
        .setMaxResults(1)
        .getResultList();
    if (userList == null || userList.isEmpty()) return null;
    return userList.get(0);
  }

}
