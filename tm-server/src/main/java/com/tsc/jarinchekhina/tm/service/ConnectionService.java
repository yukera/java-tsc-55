package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.model.User;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

  @NotNull
  private static final String HIBERNATE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";
  @NotNull
  private static final String HIBERNATE_REGION_PREFIX = "hibernate.cache.region_prefix";
  @NotNull
  private static final String HIBERNATE_CONFIG_FILE = "hibernate.cache.provider_configuration_file_resource_path";
  @NotNull
  private static final String HIBERNATE_REGION_FACTORY_CLASS = "hibernate.cache.region.factory_class";
  @NotNull
  private final IServiceLocator serviceLocator;
  @NotNull
  private final EntityManagerFactory entityManagerFactory;
  @NotNull
  private final IPropertyService propertyService;

  public ConnectionService(@NotNull final IServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
    this.propertyService = serviceLocator.getPropertyService();
    entityManagerFactory = factory();
  }

  @NotNull
  private EntityManagerFactory factory() {
    final Map<String, String> settings = new HashMap<>();
    settings.put(Environment.DRIVER, propertyService.getJdbcDriver());
    settings.put(Environment.URL, propertyService.getJdbcUrl());
    settings.put(Environment.USER, propertyService.getJdbcUsername());
    settings.put(Environment.PASS, propertyService.getJdbcPassword());
    settings.put(Environment.DIALECT, propertyService.getHibernateDialect());
    settings.put(Environment.HBM2DDL_AUTO, propertyService.getHibernateHbm2ddl());
    settings.put(Environment.SHOW_SQL, propertyService.getHibernateShow());
    settings.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS, propertyService.getHibernateLazyLoad());
    settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getHibernateSecondLevelCache());
    settings.put(Environment.USE_QUERY_CACHE, propertyService.getHibernateQueryCache());
    settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getHibernateMinimalPuts());
    settings.put(HIBERNATE_LITE_MEMBER, propertyService.getHibernateLiteMember());
    settings.put(HIBERNATE_REGION_PREFIX, propertyService.getHibernateRegionPrefix());
    settings.put(HIBERNATE_CONFIG_FILE, propertyService.getHibernateConfigFile());
    settings.put(HIBERNATE_REGION_FACTORY_CLASS, propertyService.getHibernateRegionFactoryClass());

    final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
    registryBuilder.applySettings(settings);
    final StandardServiceRegistry registry = registryBuilder.build();
    final MetadataSources sources = new MetadataSources(registry);

    sources.addAnnotatedClass(Project.class);
    sources.addAnnotatedClass(ProjectDTO.class);

    sources.addAnnotatedClass(Task.class);
    sources.addAnnotatedClass(TaskDTO.class);

    sources.addAnnotatedClass(User.class);
    sources.addAnnotatedClass(UserDTO.class);

    sources.addAnnotatedClass(Session.class);
    sources.addAnnotatedClass(SessionDTO.class);

    final Metadata metadata = sources.getMetadataBuilder().build();
    return metadata.getSessionFactoryBuilder().build();
  }

  @NotNull
  @Override
  public EntityManager getEntityManager() {
    return entityManagerFactory.createEntityManager();
  }

}
