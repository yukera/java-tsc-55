package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

  @NotNull
  private final IConnectionService connectionService;

  public ProjectService(@NotNull final IConnectionService connectionService) {
    this.connectionService = connectionService;
  }

  @Override
  public void add(@NotNull final Project project) {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.add(project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void add(@Nullable final String userId, @Nullable final Project project) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (project == null) throw new ProjectNotFoundException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.add(userId, project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  public void addAll(@NotNull final Collection<Project> collection) {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.addAll(collection);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  public void update(@NotNull final Project project) {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.update(project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void create(@Nullable final String userId, @Nullable final String name) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      @NotNull final Project project = new Project();
      project.setName(name);
      projectRepository.add(userId, project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void create(
      @Nullable final String userId,
      @Nullable final String name,
      @Nullable final String description
  ) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      @NotNull final Project project = new Project();
      project.setName(name);
      project.setDescription(description);
      projectRepository.add(userId, project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  public void clear() {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.clear();
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void clear(@Nullable final String userId) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.clear(userId);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @NotNull
  @Override
  public List<Project> findAll() {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      return projectRepository.findAll();
    } finally {
      entityManager.close();
    }
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<Project> findAll(@Nullable final String userId) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      return projectRepository.findAll(userId);
    } finally {
      entityManager.close();
    }
  }

  @NotNull
  @Override
  public Project findById(@NotNull final String id) {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      return projectRepository.findById(id);
    } finally {
      entityManager.close();
    }
  }

  @Override
  public void remove(@NotNull final Project project) {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.remove(project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void remove(@Nullable final String userId, @Nullable final Project project) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (project == null) return;
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.removeById(userId, project.getId());
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  public void removeById(@NotNull final String id) {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.removeById(id);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void removeById(@Nullable final String userId, @Nullable final String id) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(id)) throw new EmptyIdException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.removeById(userId, id);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      @Nullable final Project project = projectRepository.findByIndex(userId, index);
      projectRepository.removeById(userId, project.getId());
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void removeByName(@Nullable final String userId, @Nullable final String name) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      projectRepository.removeByName(userId, name);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void updateProjectById(
      @Nullable final String userId,
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description
  ) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(id)) throw new EmptyIdException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
    @NotNull final Project project = findById(userId, id);
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      project.setName(name);
      project.setDescription(description);
      projectRepository.update(project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @NotNull
  @Override
  @SneakyThrows
  public Project findById(@Nullable final String userId, @Nullable final String id) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(id)) throw new EmptyIdException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      return projectRepository.findById(userId, id);
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void updateProjectByIndex(
      @Nullable final String userId,
      @Nullable final Integer index,
      @Nullable final String name,
      @Nullable final String description) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
    @NotNull final Project project = findByIndex(userId, index);
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      project.setName(name);
      project.setDescription(description);
      projectRepository.update(project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @NotNull
  @Override
  @SneakyThrows
  public Project findByIndex(@Nullable final String userId, @Nullable final Integer index) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      return projectRepository.findByIndex(userId, index);
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void startProjectById(@Nullable final String userId, @Nullable final String id) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(id)) throw new EmptyIdException();
    changeProjectStatusById(userId, id, Status.IN_PROGRESS);
  }

  @Override
  @SneakyThrows
  public void changeProjectStatusById(
      @Nullable final String userId,
      @Nullable final String id,
      @Nullable final Status status
  ) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(id)) throw new EmptyIdException();
    if (status == null) throw new EmptyStatusException();
    @NotNull final Project project = findById(userId, id);
    changeProjectStatus(userId, project, status);
  }

  @SneakyThrows
  public void changeProjectStatus(
      @NotNull final String userId,
      @NotNull final Project project,
      @NotNull final Status status
  ) {
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      project.setStatus(status);
      projectRepository.update(project);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void startProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
    changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
  }

  @Override
  @SneakyThrows
  public void changeProjectStatusByIndex(
      @Nullable final String userId,
      @Nullable final Integer index,
      @Nullable final Status status
  ) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
    if (status == null) throw new EmptyStatusException();
    @NotNull final Project project = findByIndex(userId, index);
    changeProjectStatus(userId, project, status);
  }

  @Override
  @SneakyThrows
  public void startProjectByName(@Nullable final String userId, @Nullable final String name) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    changeProjectStatusByName(userId, name, Status.IN_PROGRESS);
  }

  @Override
  @SneakyThrows
  public void changeProjectStatusByName(
      @Nullable final String userId,
      @Nullable final String name,
      @Nullable final Status status
  ) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    if (status == null) throw new EmptyStatusException();
    @NotNull final Project project = findByName(userId, name);
    changeProjectStatus(userId, project, status);
  }

  @NotNull
  @Override
  @SneakyThrows
  public Project findByName(@Nullable final String userId, @Nullable final String name) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      return projectRepository.findByName(userId, name);
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void finishProjectById(@Nullable final String userId, @Nullable final String id) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(id)) throw new EmptyIdException();
    changeProjectStatusById(userId, id, Status.COMPLETED);
  }

  @Override
  @SneakyThrows
  public void finishProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
    changeProjectStatusByIndex(userId, index, Status.COMPLETED);
  }

  @Override
  @SneakyThrows
  public void finishProjectByName(@Nullable final String userId, @Nullable final String name) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(name)) throw new EmptyNameException();
    changeProjectStatusByName(userId, name, Status.COMPLETED);
  }

}
