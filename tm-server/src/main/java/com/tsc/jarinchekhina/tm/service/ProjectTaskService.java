package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

  @NotNull
  private final IConnectionService connectionService;

  public ProjectTaskService(@NotNull final IConnectionService connectionService) {
    this.connectionService = connectionService;
  }

  @NotNull
  @Override
  @SneakyThrows
  public Task bindTaskByProjectId(
      @Nullable final String userId,
      @Nullable final String projectId,
      @Nullable final String taskId
  ) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
    if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
      @NotNull final Task task = taskRepository.findById(userId, taskId);
      @NotNull final Project project = projectRepository.findById(userId, projectId);
      task.setProject(project);
      taskRepository.update(task);
      entityManager.getTransaction().commit();
      return task;
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void clearProjects(@Nullable final String userId) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
      @NotNull final List<Project> projects = projectRepository.findAll(userId);
      for (@NotNull final Project project : projects) {
        @NotNull String projectId = project.getId();
        taskRepository.removeAllByProjectId(projectId);
      }
      projectRepository.clear(userId);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @NotNull
  @Override
  @SneakyThrows
  public List<Task> findAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
      return taskRepository.findAllByProjectId(userId, projectId);
    } finally {
      entityManager.close();
    }
  }

  @Override
  @SneakyThrows
  public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
      @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
      taskRepository.removeAllByProjectId(projectId);
      projectRepository.removeById(userId, projectId);
      entityManager.getTransaction().commit();
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

  @NotNull
  @Override
  @SneakyThrows
  public Task unbindTaskByProjectId(@Nullable final String userId, @Nullable final String taskId) {
    if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
    if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
    @NotNull final EntityManager entityManager = connectionService.getEntityManager();
    try {
      entityManager.getTransaction().begin();
      @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
      @NotNull final Task task = taskRepository.findById(userId, taskId);
      task.setProject(null);
      taskRepository.update(task);
      entityManager.getTransaction().commit();
      return task;
    } catch (@NotNull final Exception e) {
      entityManager.getTransaction().rollback();
      throw e;
    } finally {
      entityManager.close();
    }
  }

}
