package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

  @NotNull
  private static final String APPLICATION_DEVELOPER_DEFAULT = "Unknown";

  @NotNull
  private static final String APPLICATION_DEVELOPER_EMAIL_DEFAULT = "Unknown";

  @NotNull
  private static final String APPLICATION_DEVELOPER_EMAIL_KEY = "APPLICATION_DEVELOPER_EMAIL";

  @NotNull
  private static final String APPLICATION_DEVELOPER_KEY = "APPLICATION_DEVELOPER";

  @NotNull
  private static final String APPLICATION_VERSION_DEFAULT = "";

  @NotNull
  private static final String APPLICATION_VERSION_KEY = "APPLICATION_VERSION";

  @NotNull
  private static final String EMPTY_DEFAULT = "";

  @NotNull
  private static final String FILE_NAME = "config.properties";

  @NotNull
  private static final String JDBC_DRIVER_DEFAULT = "com.mysql.jdbc.Driver";

  @NotNull
  private static final String JDBC_DRIVER_KEY = "JDBC_DRIVER";

  @NotNull
  private static final String JDBC_PASSWORD_DEFAULT = "";

  @NotNull
  private static final String JDBC_PASSWORD_KEY = "JDBC_PASSWORD";

  @NotNull
  private static final String JDBC_URL_DEFAULT = "jdbc:mysql://localhost:3306/task-manager";

  @NotNull
  private static final String JDBC_URL_KEY = "JDBC_URL";

  @NotNull
  private static final String JDBC_USERNAME_DEFAULT = "root";

  @NotNull
  private static final String JDBC_USERNAME_KEY = "JDBC_USERNAME";

  @NotNull
  private static final String HIBERNATE_DIALECT_DEFAULT = "org.hibernate.dialect.MySQL5InnoDBDialect";

  @NotNull
  private static final String HIBERNATE_DIALECT_KEY = "HIBERNATE_DIALECT";

  @NotNull
  private static final String HIBERNATE_HBM2DDL_DEFAULT = "update";

  @NotNull
  private static final String HIBERNATE_HBM2DDL_KEY = "HIBERNATE_HBM2DDL";

  @NotNull
  private static final String HIBERNATE_SHOW_DEFAULT = "true";

  @NotNull
  private static final String HIBERNATE_SHOW_KEY = "HIBERNATE_SHOW";

  @NotNull
  private static final String HIBERNATE_LAZY_LOAD_DEFAULT = "true";

  @NotNull
  private static final String HIBERNATE_LAZY_LOAD_KEY = "HIBERNATE_LAZY_LOAD";

  @NotNull
  private static final String HIBERNATE_SECOND_LEVEL_CACHE_DEFAULT = "true";

  @NotNull
  private static final String HIBERNATE_SECOND_LEVEL_CACHE_KEY = "HIBERNATE_SECOND_LEVEL_CACHE";

  @NotNull
  private static final String HIBERNATE_QUERY_CACHE_DEFAULT = "true";

  @NotNull
  private static final String HIBERNATE_QUERY_CACHE_KEY = "HIBERNATE_QUERY_CACHE";

  @NotNull
  private static final String HIBERNATE_MINIMAL_PUTS_DEFAULT = "true";

  @NotNull
  private static final String HIBERNATE_MINIMAL_PUTS_KEY = "HIBERNATE_MINIMAL_PUTS";

  @NotNull
  private static final String HIBERNATE_LITE_MEMBER_DEFAULT = "true";

  @NotNull
  private static final String HIBERNATE_LITE_MEMBER_KEY = "HIBERNATE_LITE_MEMBER";

  @NotNull
  private static final String HIBERNATE_REGION_PREFIX_KEY = "HIBERNATE_REGION_PREFIX";

  @NotNull
  private static final String HIBERNATE_CONFIG_FILE_KEY = "HIBERNATE_CONFIG_FILE";

  @NotNull
  private static final String HIBERNATE_REGION_FACTORY_CLASS_KEY = "HIBERNATE_REGION_FACTORY_CLASS";

  @NotNull
  private static final String PASSWORD_ITERATION_DEFAULT = "1";

  @NotNull
  private static final String PASSWORD_ITERATION_KEY = "PASSWORD_ITERATION";

  @NotNull
  private static final String PASSWORD_SECRET_DEFAULT = "";

  @NotNull
  private static final String PASSWORD_SECRET_KEY = "PASSWORD_SECRET";

  @NotNull
  private static final String SERVER_HOST_DEFAULT = "localhost";

  @NotNull
  private static final String SERVER_HOST_KEY = "SERVER_HOST";

  @NotNull
  private static final String SERVER_PORT_DEFAULT = "8080";

  @NotNull
  private static final String SERVER_PORT_KEY = "SERVER_PORT";

  @NotNull
  private static final String SESSION_ITERATION_DEFAULT = "1";

  @NotNull
  private static final String SESSION_ITERATION_KEY = "SESSION_ITERATION";

  @NotNull
  private static final String SESSION_SECRET_DEFAULT = "";

  @NotNull
  private static final String SESSION_SECRET_KEY = "SESSION_SECRET";

  @NotNull
  private final Properties properties = new Properties();

  @SneakyThrows
  public PropertyService() {
    @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
    if (inputStream == null) return;
    properties.load(inputStream);
    inputStream.close();
  }

  @NotNull
  @Override
  public String getDeveloper() {
    return properties.getProperty(APPLICATION_DEVELOPER_KEY, APPLICATION_DEVELOPER_DEFAULT);
  }

  @NotNull
  @Override
  public String getDeveloperEmail() {
    return properties.getProperty(APPLICATION_DEVELOPER_EMAIL_KEY, APPLICATION_DEVELOPER_EMAIL_DEFAULT);
  }

  @NotNull
  @Override
  public String getJdbcDriver() {
    return getValue(JDBC_DRIVER_KEY, JDBC_DRIVER_DEFAULT);
  }

  @NotNull
  @Override
  public String getValue(@Nullable final String key, @Nullable final String defaultValue) {
    if (DataUtil.isEmpty(key)) return EMPTY_DEFAULT;
    if (defaultValue == null) return EMPTY_DEFAULT;
    if (System.getProperties().containsKey(key))
      return System.getProperty(key);
    if (System.getenv().containsKey(key))
      return System.getenv(key);
    return properties.getProperty(key, defaultValue);
  }

  @NotNull
  @Override
  public String getJdbcPassword() {
    return getValue(JDBC_PASSWORD_KEY, JDBC_PASSWORD_DEFAULT);
  }

  @NotNull
  @Override
  public String getJdbcUrl() {
    return getValue(JDBC_URL_KEY, JDBC_URL_DEFAULT);
  }

  @NotNull
  @Override
  public String getJdbcUsername() {
    return getValue(JDBC_USERNAME_KEY, JDBC_USERNAME_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateDialect() {
    return getValue(HIBERNATE_DIALECT_KEY, HIBERNATE_DIALECT_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateHbm2ddl() {
    return getValue(HIBERNATE_HBM2DDL_KEY, HIBERNATE_HBM2DDL_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateShow() {
    return getValue(HIBERNATE_SHOW_KEY, HIBERNATE_SHOW_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateLazyLoad() {
    return getValue(HIBERNATE_LAZY_LOAD_KEY, HIBERNATE_LAZY_LOAD_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateSecondLevelCache() {
    return getValue(HIBERNATE_SECOND_LEVEL_CACHE_KEY, HIBERNATE_SECOND_LEVEL_CACHE_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateQueryCache() {
    return getValue(HIBERNATE_QUERY_CACHE_KEY, HIBERNATE_QUERY_CACHE_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateMinimalPuts() {
    return getValue(HIBERNATE_MINIMAL_PUTS_KEY, HIBERNATE_MINIMAL_PUTS_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateLiteMember() {
    return getValue(HIBERNATE_LITE_MEMBER_KEY, HIBERNATE_LITE_MEMBER_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateRegionPrefix() {
    return getValue(HIBERNATE_REGION_PREFIX_KEY, EMPTY_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateConfigFile() {
    return getValue(HIBERNATE_CONFIG_FILE_KEY, EMPTY_DEFAULT);
  }

  @NotNull
  @Override
  public String getHibernateRegionFactoryClass() {
    return getValue(HIBERNATE_REGION_FACTORY_CLASS_KEY, EMPTY_DEFAULT);
  }

  @NotNull
  @Override
  public Integer getPasswordIteration() {
    @NotNull String value = getValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    return Integer.parseInt(value);
  }

  @NotNull
  @Override
  public String getPasswordSecret() {
    return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
  }

  @NotNull
  @Override
  public String getServerHost() {
    return getValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
  }

  @NotNull
  @Override
  public Integer getServerPort() {
    @NotNull String value = getValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    return Integer.parseInt(value);
  }

  @NotNull
  @Override
  public Integer getSessionIteration() {
    @NotNull String value = getValue(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT);
    return Integer.parseInt(value);
  }

  @NotNull
  @Override
  public String getSessionSecret() {
    return getValue(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
  }

  @NotNull
  @Override
  public String getVersion() {
    return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
  }

}
