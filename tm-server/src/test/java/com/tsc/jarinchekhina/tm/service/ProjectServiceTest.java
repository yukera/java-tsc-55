package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;


public class ProjectServiceTest {

  @NotNull
  private static final Bootstrap bootstrap = new Bootstrap();

  @Nullable
  private static User user;

  @Nullable
  private static String userId;

  @BeforeClass
  public static void beforeClass() {
    user = bootstrap.getUserService().findByLogin("auto");
    if (user == null) user = bootstrap.getUserService().create("auto", "auto");
    userId = user.getId();
  }

  @AfterClass
  public static void afterClass() {
    bootstrap.getProjectTaskService().clearProjects(userId);
    bootstrap.getUserService().removeByLogin("auto");
  }

  @After
  public void after() {
    bootstrap.getProjectService().clear(userId);
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindAllNoUserId() {
    bootstrap.getProjectService().findAll("");
  }

  @Test(expected = AccessDeniedException.class)
  public void testAddNoUserId() {
    bootstrap.getProjectService().add("", null);
  }

  @Test(expected = AccessDeniedException.class)
  public void testCreateNoUserId() {
    bootstrap.getProjectService().create("", "AUTO Project");
  }

  @Test(expected = EmptyNameException.class)
  public void testCreateNoName() {
    bootstrap.getProjectService().create(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testCreateWithDescriptionNoUserId() {
    bootstrap.getProjectService().create("", "AUTO Project", "Desc");
  }

  @Test(expected = EmptyNameException.class)
  public void testCreateWithDescriptionNoName() {
    bootstrap.getProjectService().create(userId, "", "Desc");
  }

  @Test(expected = EmptyDescriptionException.class)
  public void testCreateWithDescriptionNoDescription() {
    bootstrap.getProjectService().create(userId, "AUTO Project", "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveNoUserId() {
    bootstrap.getProjectService().remove("", null);
  }

  @Test
  public void testCreateFindAllRemove() {
    bootstrap.getProjectService().create(userId, "AUTO Project 2");
    bootstrap.getProjectService().create(userId, "AUTO Project 1", "desc");
    @NotNull List<Project> projectList = bootstrap.getProjectService().findAll(userId);
    Assert.assertEquals(2, projectList.size());

    bootstrap.getProjectService().remove(userId, projectList.get(0));
    bootstrap.getProjectService().remove(userId, projectList.get(1));
    projectList = bootstrap.getProjectService().findAll(userId);
    Assert.assertEquals(0, projectList.size());
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindByIdNoUserId() {
    bootstrap.getProjectService().findById("", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testFindByIdNoId() {
    bootstrap.getProjectService().findById(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindByIndexNoUserId() {
    bootstrap.getProjectService().findByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testFindByIndexNoId() {
    bootstrap.getProjectService().findByIndex(userId, null);
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindByNameNoUserId() {
    bootstrap.getProjectService().findByName("", "1");
  }

  @Test(expected = EmptyNameException.class)
  public void testFindByNameNoName() {
    bootstrap.getProjectService().findByName(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveByIdNoUserId() {
    bootstrap.getProjectService().removeById("", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testRemoveByIdNoId() {
    bootstrap.getProjectService().removeById(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveByIndexNoUserId() {
    bootstrap.getProjectService().removeByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testRemoveByIndexNoId() {
    bootstrap.getProjectService().removeByIndex(userId, null);
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveByNameNoUserId() {
    bootstrap.getProjectService().removeByName("", "1");
  }

  @Test(expected = EmptyNameException.class)
  public void testRemoveByNameNoName() {
    bootstrap.getProjectService().removeByName(userId, "");
  }

  @Test
  public void testCreateFindRemove() {
    bootstrap.getProjectService().create(userId, "AUTO Project CFR Id");
    bootstrap.getProjectService().create(userId, "AUTO Project CFR Index");
    bootstrap.getProjectService().create(userId, "AUTO Project CFR Name");

    @NotNull final Project projectById = bootstrap.getProjectService().findByName(userId, "AUTO Project CFR Id");
    @NotNull final Project projectByName = bootstrap.getProjectService().findByName(userId, "AUTO Project CFR Name");
    @NotNull Project projectFound = bootstrap.getProjectService().findById(userId, projectById.getId());
    Assert.assertNotNull(projectFound);
    Assert.assertEquals("AUTO Project CFR Id", projectFound.getName());

    projectFound = bootstrap.getProjectService().findByName(userId, "AUTO Project CFR Name");
    Assert.assertNotNull(projectFound);
    Assert.assertEquals(projectByName.getId(), projectFound.getId());

    bootstrap.getProjectService().removeById(userId, projectById.getId());
    bootstrap.getProjectService().removeByName(userId, "AUTO Project CFR Name");
    bootstrap.getProjectService().removeByIndex(userId, 1);

    @NotNull final List<Project> projectList = bootstrap.getProjectService().findAll(userId);
    Assert.assertEquals(0, projectList.size());
  }

  @Test(expected = AccessDeniedException.class)
  public void testUpdateProjectByIdNoUserId() {
    bootstrap.getProjectService().updateProjectById("", "1", "123", "456");
  }

  @Test(expected = EmptyIdException.class)
  public void testUpdateProjectByIdNoId() {
    bootstrap.getProjectService().updateProjectById(userId, "", "123", "456");
  }

  @Test(expected = EmptyNameException.class)
  public void testUpdateProjectByIdNoName() {
    bootstrap.getProjectService().updateProjectById(userId, "1", "", "456");
  }

  @Test(expected = EmptyDescriptionException.class)
  public void testUpdateProjectByIdNoDescription() {
    bootstrap.getProjectService().updateProjectById(userId, "1", "123", "");
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testUpdateProjectByIdNoProject() {
    bootstrap.getProjectService().updateProjectById(userId, "1", "123", "456");
  }

  @Test(expected = AccessDeniedException.class)
  public void testUpdateProjectByIndexNoUserId() {
    bootstrap.getProjectService().updateProjectByIndex("", 1, "123", "456");
  }

  @Test(expected = IndexIncorrectException.class)
  public void testUpdateProjectByIndexNoIndex() {
    bootstrap.getProjectService().updateProjectByIndex(userId, null, "123", "456");
  }

  @Test(expected = EmptyNameException.class)
  public void testUpdateProjectByIndexNoName() {
    bootstrap.getProjectService().updateProjectByIndex(userId, 1, "", "456");
  }

  @Test(expected = EmptyDescriptionException.class)
  public void testUpdateProjectByIndexNoDescription() {
    bootstrap.getProjectService().updateProjectByIndex(userId, 1, "123", "");
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testUpdateProjectByIndexNoProject() {
    bootstrap.getProjectService().updateProjectByIndex(userId, 1, "123", "456");
  }

  @Test
  public void testUpdateProject() {
    bootstrap.getProjectService().create(userId, "AUTO", "AUTO");
    bootstrap.getProjectService().create(userId, "AUTO 2", "AUTO 2");
    @NotNull Project projectById = bootstrap.getProjectService().findByName(userId, "AUTO");

    bootstrap.getProjectService().updateProjectById(userId, projectById.getId(), "AUTO1", "AUTO1D");
    projectById = bootstrap.getProjectService().findByName(userId, "AUTO1");
    Assert.assertEquals("AUTO1D", projectById.getDescription());

    bootstrap.getProjectService().updateProjectByIndex(userId, 1, "AUTO2", "AUTO2D");
    @NotNull final Project projectByName = bootstrap.getProjectService().findByName(userId, "AUTO2");
    Assert.assertEquals("AUTO2D", projectByName.getDescription());

    bootstrap.getProjectService().removeById(userId, projectById.getId());
    bootstrap.getProjectService().removeByIndex(userId, 1);
  }

  @Test(expected = AccessDeniedException.class)
  public void testStartProjectByIdNoUserId() {
    bootstrap.getProjectService().startProjectById("", "");
  }

  @Test(expected = EmptyIdException.class)
  public void testStartProjectByIdNoId() {
    bootstrap.getProjectService().startProjectById(userId, "");
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testStartProjectByIdNoProject() {
    bootstrap.getProjectService().startProjectById(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testStartProjectByIndexNoUserId() {
    bootstrap.getProjectService().startProjectByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testStartProjectByIndexNoIndex() {
    bootstrap.getProjectService().startProjectByIndex(userId, null);
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testStartProjectByIndexNoProject() {
    bootstrap.getProjectService().startProjectByIndex(userId, 1);
  }

  @Test(expected = AccessDeniedException.class)
  public void testStartProjectByNameNoUserId() {
    bootstrap.getProjectService().startProjectByName("", "123");
  }

  @Test(expected = EmptyNameException.class)
  public void testStartProjectByNameNoName() {
    bootstrap.getProjectService().startProjectByName(userId, null);
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testStartProjectByNameNoProject() {
    bootstrap.getProjectService().startProjectByName(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testFinishProjectByIdNoUserId() {
    bootstrap.getProjectService().finishProjectById("", "");
  }

  @Test(expected = EmptyIdException.class)
  public void testFinishProjectByIdNoId() {
    bootstrap.getProjectService().finishProjectById(userId, "");
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testFinishProjectByIdNoProject() {
    bootstrap.getProjectService().finishProjectById(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testFinishProjectByIndexNoUserId() {
    bootstrap.getProjectService().finishProjectByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testFinishProjectByIndexNoIndex() {
    bootstrap.getProjectService().finishProjectByIndex(userId, null);
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testFinishProjectByIndexNoProject() {
    bootstrap.getProjectService().finishProjectByIndex(userId, 1);
  }

  @Test(expected = AccessDeniedException.class)
  public void testFinishProjectByNameNoUserId() {
    bootstrap.getProjectService().finishProjectByName("", "123");
  }

  @Test(expected = EmptyNameException.class)
  public void testFinishProjectByNameNoName() {
    bootstrap.getProjectService().finishProjectByName(userId, null);
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testFinishProjectByNameNoProject() {
    bootstrap.getProjectService().finishProjectByName(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testChangeProjectStatusByIdNoUserId() {
    bootstrap.getProjectService().changeProjectStatusById("", "", Status.IN_PROGRESS);
  }

  @Test(expected = EmptyIdException.class)
  public void testChangeProjectStatusByIdNoId() {
    bootstrap.getProjectService().changeProjectStatusById(userId, "", Status.IN_PROGRESS);
  }

  @Test(expected = EmptyStatusException.class)
  public void testChangeProjectStatusByIdNoStatus() {
    bootstrap.getProjectService().changeProjectStatusById(userId, "1", null);
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testChangeProjectStatusByIdNoProject() {
    bootstrap.getProjectService().changeProjectStatusById(userId, "1", Status.IN_PROGRESS);
  }

  @Test(expected = AccessDeniedException.class)
  public void testChangeProjectStatusByIndexNoUserId() {
    bootstrap.getProjectService().changeProjectStatusByIndex("", 1, Status.IN_PROGRESS);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testChangeProjectStatusByIndexNoIndex() {
    bootstrap.getProjectService().changeProjectStatusByIndex(userId, null, Status.IN_PROGRESS);
  }

  @Test(expected = EmptyStatusException.class)
  public void testChangeProjectStatusByIndexNoStatus() {
    bootstrap.getProjectService().changeProjectStatusByIndex(userId, 1, null);
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testChangeProjectStatusByIndexNoProject() {
    bootstrap.getProjectService().changeProjectStatusByIndex(userId, 1, Status.IN_PROGRESS);
  }

  @Test(expected = AccessDeniedException.class)
  public void testChangeProjectStatusByNameNoUserId() {
    bootstrap.getProjectService().changeProjectStatusByName("", "123", Status.IN_PROGRESS);
  }

  @Test(expected = EmptyNameException.class)
  public void testChangeProjectStatusByNameNoName() {
    bootstrap.getProjectService().changeProjectStatusByName(userId, null, Status.IN_PROGRESS);
  }

  @Test(expected = EmptyStatusException.class)
  public void testChangeProjectStatusByNameNoStatus() {
    bootstrap.getProjectService().changeProjectStatusByName(userId, "1", null);
  }

  @Test(expected = ProjectNotFoundException.class)
  public void testChangeProjectStatusByNameNoProject() {
    bootstrap.getProjectService().changeProjectStatusByName(userId, "1", Status.IN_PROGRESS);
  }

  @Test
  public void testStatusField() {
    bootstrap.getProjectService().create(userId, "AUTO Project Id");
    @NotNull final Project projectById = bootstrap.getProjectService().findByName(userId, "AUTO Project Id");
    Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());
    bootstrap.getProjectService().create(userId, "AUTO Project Name");
    @NotNull final Project projectByName = bootstrap.getProjectService().findByName(userId, "AUTO Project Name");
    Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());

    bootstrap.getProjectService().startProjectById(userId, projectById.getId());
    @NotNull Project project = bootstrap.getProjectService().findByName(userId, "AUTO Project Id");
    Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    bootstrap.getProjectService().startProjectByName(userId, "AUTO Project Name");
    project = bootstrap.getProjectService().findByName(userId, "AUTO Project Name");
    Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

    bootstrap.getProjectService().finishProjectById(userId, projectById.getId());
    project = bootstrap.getProjectService().findByName(userId, "AUTO Project Id");
    Assert.assertEquals(Status.COMPLETED, project.getStatus());
    bootstrap.getProjectService().finishProjectByName(userId, "AUTO Project Name");
    project = bootstrap.getProjectService().findByName(userId, "AUTO Project Name");
    Assert.assertEquals(Status.COMPLETED, project.getStatus());

    bootstrap.getProjectService().changeProjectStatusById(userId, projectById.getId(), Status.NOT_STARTED);
    project = bootstrap.getProjectService().findByName(userId, "AUTO Project Id");
    Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    bootstrap.getProjectService().changeProjectStatusByName(userId, "AUTO Project Name", Status.NOT_STARTED);
    project = bootstrap.getProjectService().findByName(userId, "AUTO Project Name");
    Assert.assertEquals(Status.NOT_STARTED, project.getStatus());

    bootstrap.getProjectService().remove(userId, projectById);
    bootstrap.getProjectService().remove(userId, projectByName);
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveProjectByIdNoUserId() {
    bootstrap.getProjectTaskService().removeProjectById("", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testRemoveProjectByIdNoId() {
    bootstrap.getProjectTaskService().removeProjectById(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testClearProjectsNoUserId() {
    bootstrap.getProjectTaskService().clearProjects("");
  }

  @Test
  public void testRemoveClearProjects() {
    bootstrap.getProjectService().create(userId, "AUTO Pr 1");
    @NotNull final Project projectFirst = bootstrap.getProjectService().findByName(userId, "AUTO Pr 1");
    bootstrap.getProjectService().create(userId, "AUTO Pr 2");
    @NotNull final Project projectSecond = bootstrap.getProjectService().findByName(userId, "AUTO Pr 2");
    bootstrap.getProjectService().create(userId, "AUTO Pr 3");
    bootstrap.getTaskService().create(userId, "AUTO T 1");
    @NotNull final Task taskFirst = bootstrap.getTaskService().findByName(userId, "AUTO T 1");
    bootstrap.getTaskService().create(userId, "AUTO T 2");
    @NotNull final Task taskSecond = bootstrap.getTaskService().findByName(userId, "AUTO T 2");
    bootstrap.getTaskService().create(userId, "AUTO T 3");
    @NotNull final Task taskThird = bootstrap.getTaskService().findByName(userId, "AUTO T 3");

    bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectFirst.getId(), taskFirst.getId());
    bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectSecond.getId(), taskSecond.getId());
    bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectSecond.getId(), taskThird.getId());

    bootstrap.getProjectTaskService().removeProjectById(userId, projectFirst.getId());
    @NotNull Project project = bootstrap.getProjectService().findByName(userId, "AUTO Pr 2");
    Assert.assertNotNull(project);
    @NotNull Task task = bootstrap.getTaskService().findByName(userId, "AUTO T 2");
    Assert.assertNotNull(task);

    bootstrap.getProjectTaskService().clearProjects(userId);
  }

}
