package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;


public class TaskServiceTest {

  @NotNull
  private static final Bootstrap bootstrap = new Bootstrap();

  @Nullable
  private static User user;

  @Nullable
  private static String userId;

  @BeforeClass
  public static void beforeClass() {
    user = bootstrap.getUserService().findByLogin("auto");
    if (user == null) user = bootstrap.getUserService().create("auto", "auto");
    userId = user.getId();
  }

  @AfterClass
  public static void afterClass() {
    bootstrap.getUserService().removeByLogin("auto");
  }

  @After
  public void after() {
    bootstrap.getTaskService().clear(userId);
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindAllNoUserId() {
    bootstrap.getTaskService().findAll("");
  }

  @Test(expected = AccessDeniedException.class)
  public void testAddNoUserId() {
    bootstrap.getTaskService().add("", null);
  }

  @Test(expected = AccessDeniedException.class)
  public void testCreateNoUserId() {
    bootstrap.getTaskService().create("", "AUTO Task");
  }

  @Test(expected = EmptyNameException.class)
  public void testCreateNoName() {
    bootstrap.getTaskService().create(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testCreateWithDescriptionNoUserId() {
    bootstrap.getTaskService().create("", "AUTO Task", "Desc");
  }

  @Test(expected = EmptyNameException.class)
  public void testCreateWithDescriptionNoName() {
    bootstrap.getTaskService().create(userId, "", "Desc");
  }

  @Test(expected = EmptyDescriptionException.class)
  public void testCreateWithDescriptionNoDescription() {
    bootstrap.getTaskService().create(userId, "AUTO Task", "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveNoUserId() {
    bootstrap.getTaskService().remove("", null);
  }

  @Test
  public void testCreateFindAllRemove() {
    @Nullable Task task = new Task();
    task.setName("AUTO Task 3");
    task.setUser(user);
    bootstrap.getTaskService().add(userId, task);

    bootstrap.getTaskService().create(userId, "AUTO Task 2");
    bootstrap.getTaskService().create(userId, "AUTO Task 1", "desc");
    @NotNull List<Task> taskList = bootstrap.getTaskService().findAll(userId);
    Assert.assertEquals(3, taskList.size());

    bootstrap.getTaskService().remove(userId, taskList.get(0));
    bootstrap.getTaskService().remove(userId, taskList.get(1));
    bootstrap.getTaskService().remove(userId, taskList.get(2));
    taskList = bootstrap.getTaskService().findAll(userId);
    Assert.assertEquals(0, taskList.size());
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindByIdNoUserId() {
    bootstrap.getTaskService().findById("", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testFindByIdNoId() {
    bootstrap.getTaskService().findById(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindByIndexNoUserId() {
    bootstrap.getTaskService().findByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testFindByIndexNoId() {
    bootstrap.getTaskService().findByIndex(userId, null);
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindByNameNoUserId() {
    bootstrap.getTaskService().findByName("", "1");
  }

  @Test(expected = EmptyNameException.class)
  public void testFindByNameNoName() {
    bootstrap.getTaskService().findByName(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveByIdNoUserId() {
    bootstrap.getTaskService().removeById("", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testRemoveByIdNoId() {
    bootstrap.getTaskService().removeById(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveByIndexNoUserId() {
    bootstrap.getTaskService().removeByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testRemoveByIndexNoId() {
    bootstrap.getTaskService().removeByIndex(userId, null);
  }

  @Test(expected = AccessDeniedException.class)
  public void testRemoveByNameNoUserId() {
    bootstrap.getTaskService().removeByName("", "1");
  }

  @Test(expected = EmptyNameException.class)
  public void testRemoveByNameNoName() {
    bootstrap.getTaskService().removeByName(userId, "");
  }

  @Test
  public void testCreateFindRemove() {
    bootstrap.getTaskService().create(userId, "AUTO Task CFR Id");
    bootstrap.getTaskService().create(userId, "AUTO Task CFR Index");
    bootstrap.getTaskService().create(userId, "AUTO Task CFR Name");

    @NotNull final Task taskById = bootstrap.getTaskService().findByName(userId, "AUTO Task CFR Id");
    @NotNull final Task taskByName = bootstrap.getTaskService().findByName(userId, "AUTO Task CFR Name");
    @NotNull Task taskFound = bootstrap.getTaskService().findById(userId, taskById.getId());
    Assert.assertNotNull(taskFound);
    Assert.assertEquals("AUTO Task CFR Id", taskFound.getName());

    taskFound = bootstrap.getTaskService().findByIndex(userId, 1);
    @Nullable List<Task> taskList = bootstrap.getTaskService().findAll(userId);
    Assert.assertNotNull(taskFound);
    Assert.assertEquals(taskList.get(0).getId(), taskFound.getId());

    taskFound = bootstrap.getTaskService().findByName(userId, "AUTO Task CFR Name");
    Assert.assertNotNull(taskFound);
    Assert.assertEquals(taskByName.getId(), taskFound.getId());

    bootstrap.getTaskService().removeById(userId, taskById.getId());
    bootstrap.getTaskService().removeByName(userId, "AUTO Task CFR Name");
    bootstrap.getTaskService().removeByIndex(userId, 1);

    taskList = bootstrap.getTaskService().findAll(userId);
    Assert.assertEquals(0, taskList.size());
  }

  @Test(expected = AccessDeniedException.class)
  public void testUpdateTaskByIdNoUserId() {
    bootstrap.getTaskService().updateTaskById("", "1", "123", "456");
  }

  @Test(expected = EmptyIdException.class)
  public void testUpdateTaskByIdNoId() {
    bootstrap.getTaskService().updateTaskById(userId, "", "123", "456");
  }

  @Test(expected = EmptyNameException.class)
  public void testUpdateTaskByIdNoName() {
    bootstrap.getTaskService().updateTaskById(userId, "1", "", "456");
  }

  @Test(expected = EmptyDescriptionException.class)
  public void testUpdateTaskByIdNoDescription() {
    bootstrap.getTaskService().updateTaskById(userId, "1", "123", "");
  }

  @Test(expected = TaskNotFoundException.class)
  public void testUpdateTaskByIdNoTask() {
    bootstrap.getTaskService().updateTaskById(userId, "1", "123", "456");
  }

  @Test(expected = AccessDeniedException.class)
  public void testUpdateTaskByIndexNoUserId() {
    bootstrap.getTaskService().updateTaskByIndex("", 1, "123", "456");
  }

  @Test(expected = IndexIncorrectException.class)
  public void testUpdateTaskByIndexNoIndex() {
    bootstrap.getTaskService().updateTaskByIndex(userId, null, "123", "456");
  }

  @Test(expected = EmptyNameException.class)
  public void testUpdateTaskByIndexNoName() {
    bootstrap.getTaskService().updateTaskByIndex(userId, 1, "", "456");
  }

  @Test(expected = EmptyDescriptionException.class)
  public void testUpdateTaskByIndexNoDescription() {
    bootstrap.getTaskService().updateTaskByIndex(userId, 1, "123", "");
  }

  @Test(expected = TaskNotFoundException.class)
  public void testUpdateTaskByIndexNoTask() {
    bootstrap.getTaskService().updateTaskByIndex(userId, 1, "123", "456");
  }

  @Test
  public void testUpdateTask() {
    bootstrap.getTaskService().create(userId, "AUTO", "AUTO");
    bootstrap.getTaskService().create(userId, "AUTO 2", "AUTO 2");
    @NotNull Task taskById = bootstrap.getTaskService().findByName(userId, "AUTO");

    bootstrap.getTaskService().updateTaskById(userId, taskById.getId(), "AUTO1", "AUTO1D");
    taskById = bootstrap.getTaskService().findByName(userId, "AUTO1");
    Assert.assertEquals("AUTO1D", taskById.getDescription());

    bootstrap.getTaskService().updateTaskByIndex(userId, 1, "AUTO2", "AUTO2D");
    @NotNull final Task taskByName = bootstrap.getTaskService().findByName(userId, "AUTO2");
    Assert.assertEquals("AUTO2D", taskByName.getDescription());

    bootstrap.getTaskService().removeByIndex(userId, 1);
    bootstrap.getTaskService().removeByIndex(userId, 1);
  }

  @Test(expected = AccessDeniedException.class)
  public void testStartTaskByIdNoUserId() {
    bootstrap.getTaskService().startTaskById("", "");
  }

  @Test(expected = EmptyIdException.class)
  public void testStartTaskByIdNoId() {
    bootstrap.getTaskService().startTaskById(userId, "");
  }

  @Test(expected = TaskNotFoundException.class)
  public void testStartTaskByIdNoTask() {
    bootstrap.getTaskService().startTaskById(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testStartTaskByIndexNoUserId() {
    bootstrap.getTaskService().startTaskByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testStartTaskByIndexNoIndex() {
    bootstrap.getTaskService().startTaskByIndex(userId, null);
  }

  @Test(expected = TaskNotFoundException.class)
  public void testStartTaskByIndexNoTask() {
    bootstrap.getTaskService().startTaskByIndex(userId, 1);
  }

  @Test(expected = AccessDeniedException.class)
  public void testStartTaskByNameNoUserId() {
    bootstrap.getTaskService().startTaskByName("", "123");
  }

  @Test(expected = EmptyNameException.class)
  public void testStartTaskByNameNoName() {
    bootstrap.getTaskService().startTaskByName(userId, null);
  }

  @Test(expected = TaskNotFoundException.class)
  public void testStartTaskByNameNoTask() {
    bootstrap.getTaskService().startTaskByName(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testFinishTaskByIdNoUserId() {
    bootstrap.getTaskService().finishTaskById("", "");
  }

  @Test(expected = EmptyIdException.class)
  public void testFinishTaskByIdNoId() {
    bootstrap.getTaskService().finishTaskById(userId, "");
  }

  @Test(expected = TaskNotFoundException.class)
  public void testFinishTaskByIdNoTask() {
    bootstrap.getTaskService().finishTaskById(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testFinishTaskByIndexNoUserId() {
    bootstrap.getTaskService().finishTaskByIndex("", 1);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testFinishTaskByIndexNoIndex() {
    bootstrap.getTaskService().finishTaskByIndex(userId, null);
  }

  @Test(expected = TaskNotFoundException.class)
  public void testFinishTaskByIndexNoTask() {
    bootstrap.getTaskService().finishTaskByIndex(userId, 1);
  }

  @Test(expected = AccessDeniedException.class)
  public void testFinishTaskByNameNoUserId() {
    bootstrap.getTaskService().finishTaskByName("", "123");
  }

  @Test(expected = EmptyNameException.class)
  public void testFinishTaskByNameNoName() {
    bootstrap.getTaskService().finishTaskByName(userId, null);
  }

  @Test(expected = TaskNotFoundException.class)
  public void testFinishTaskByNameNoTask() {
    bootstrap.getTaskService().finishTaskByName(userId, "1");
  }

  @Test(expected = AccessDeniedException.class)
  public void testChangeTaskStatusByIdNoUserId() {
    bootstrap.getTaskService().changeTaskStatusById("", "", Status.IN_PROGRESS);
  }

  @Test(expected = EmptyIdException.class)
  public void testChangeTaskStatusByIdNoId() {
    bootstrap.getTaskService().changeTaskStatusById(userId, "", Status.IN_PROGRESS);
  }

  @Test(expected = EmptyStatusException.class)
  public void testChangeTaskStatusByIdNoStatus() {
    bootstrap.getTaskService().changeTaskStatusById(userId, "1", null);
  }

  @Test(expected = TaskNotFoundException.class)
  public void testChangeTaskStatusByIdNoTask() {
    bootstrap.getTaskService().changeTaskStatusById(userId, "1", Status.IN_PROGRESS);
  }

  @Test(expected = AccessDeniedException.class)
  public void testChangeTaskStatusByIndexNoUserId() {
    bootstrap.getTaskService().changeTaskStatusByIndex("", 1, Status.IN_PROGRESS);
  }

  @Test(expected = IndexIncorrectException.class)
  public void testChangeTaskStatusByIndexNoIndex() {
    bootstrap.getTaskService().changeTaskStatusByIndex(userId, null, Status.IN_PROGRESS);
  }

  @Test(expected = EmptyStatusException.class)
  public void testChangeTaskStatusByIndexNoStatus() {
    bootstrap.getTaskService().changeTaskStatusByIndex(userId, 1, null);
  }

  @Test(expected = TaskNotFoundException.class)
  public void testChangeTaskStatusByIndexNoTask() {
    bootstrap.getTaskService().changeTaskStatusByIndex(userId, 1, Status.IN_PROGRESS);
  }

  @Test(expected = AccessDeniedException.class)
  public void testChangeTaskStatusByNameNoUserId() {
    bootstrap.getTaskService().changeTaskStatusByName("", "123", Status.IN_PROGRESS);
  }

  @Test(expected = EmptyNameException.class)
  public void testChangeTaskStatusByNameNoName() {
    bootstrap.getTaskService().changeTaskStatusByName(userId, null, Status.IN_PROGRESS);
  }

  @Test(expected = EmptyStatusException.class)
  public void testChangeTaskStatusByNameNoStatus() {
    bootstrap.getTaskService().changeTaskStatusByName(userId, "1", null);
  }

  @Test(expected = TaskNotFoundException.class)
  public void testChangeTaskStatusByNameNoTask() {
    bootstrap.getTaskService().changeTaskStatusByName(userId, "1", Status.IN_PROGRESS);
  }

  @Test
  public void testStatusField() {
    bootstrap.getTaskService().create(userId, "AUTO Task Id");
    @NotNull final Task taskById = bootstrap.getTaskService().findByName(userId, "AUTO Task Id");
    Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());
    bootstrap.getTaskService().create(userId, "AUTO Task Name");
    @NotNull final Task taskByName = bootstrap.getTaskService().findByName(userId, "AUTO Task Name");
    Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());

    bootstrap.getTaskService().startTaskById(userId, taskById.getId());
    @NotNull Task task = bootstrap.getTaskService().findByName(userId, "AUTO Task Id");
    Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    bootstrap.getTaskService().startTaskByName(userId, "AUTO Task Name");
    task = bootstrap.getTaskService().findByName(userId, "AUTO Task Name");
    Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

    bootstrap.getTaskService().finishTaskById(userId, taskById.getId());
    task = bootstrap.getTaskService().findByName(userId, "AUTO Task Id");
    Assert.assertEquals(Status.COMPLETED, task.getStatus());
    bootstrap.getTaskService().finishTaskByName(userId, "AUTO Task Name");
    task = bootstrap.getTaskService().findByName(userId, "AUTO Task Name");
    Assert.assertEquals(Status.COMPLETED, task.getStatus());

    bootstrap.getTaskService().changeTaskStatusById(userId, taskById.getId(), Status.NOT_STARTED);
    task = bootstrap.getTaskService().findByName(userId, "AUTO Task Id");
    Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    bootstrap.getTaskService().changeTaskStatusByName(userId, "AUTO Task Name", Status.NOT_STARTED);
    task = bootstrap.getTaskService().findByName(userId, "AUTO Task Name");
    Assert.assertEquals(Status.NOT_STARTED, task.getStatus());

    bootstrap.getTaskService().remove(userId, taskById);
    bootstrap.getTaskService().remove(userId, taskByName);
  }

  @Test(expected = AccessDeniedException.class)
  public void testFindAllByProjectIdNoUserId() {
    bootstrap.getProjectTaskService().findAllTaskByProjectId("", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testFindAllByProjectIdNoProjectId() {
    bootstrap.getProjectTaskService().findAllTaskByProjectId(userId, "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testBindTaskNoUserId() {
    bootstrap.getProjectTaskService().bindTaskByProjectId("", "1", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testBindTaskNoProjectId() {
    bootstrap.getProjectTaskService().bindTaskByProjectId(userId, "", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testBindTaskNoTaskId() {
    bootstrap.getProjectTaskService().bindTaskByProjectId(userId, "1", "");
  }

  @Test(expected = AccessDeniedException.class)
  public void testUnbindTaskNoUserId() {
    bootstrap.getProjectTaskService().unbindTaskByProjectId("", "1");
  }

  @Test(expected = EmptyIdException.class)
  public void testUnbindTaskNoTaskId() {
    bootstrap.getProjectTaskService().unbindTaskByProjectId(userId, "");
  }

  @Test
  public void testBindUnbindTask() {
    bootstrap.getProjectService().create(userId, "AUTO Project");
    @NotNull final Project project = bootstrap.getProjectService().findByName(userId, "AUTO Project");
    bootstrap.getTaskService().create(userId, "AUTO Task 1");
    @NotNull final Task taskFirst = bootstrap.getTaskService().findByName(userId, "AUTO Task 1");
    bootstrap.getTaskService().create(userId, "AUTO Task 2");
    @NotNull final Task taskSecond = bootstrap.getTaskService().findByName(userId, "AUTO Task 2");

    bootstrap.getProjectTaskService().bindTaskByProjectId(userId, project.getId(), taskFirst.getId());
    bootstrap.getProjectTaskService().bindTaskByProjectId(userId, project.getId(), taskSecond.getId());
    @NotNull List<Task> taskList = bootstrap.getProjectTaskService().findAllTaskByProjectId(userId, project.getId());
    Assert.assertEquals(2, taskList.size());
    Assert.assertEquals(project.getId(), taskList.get(0).getProject().getId());
    Assert.assertEquals(project.getId(), taskList.get(1).getProject().getId());

    bootstrap.getProjectTaskService().unbindTaskByProjectId(userId, taskFirst.getId());
    taskList = bootstrap.getProjectTaskService().findAllTaskByProjectId(userId, project.getId());
    Assert.assertEquals(1, taskList.size());

    bootstrap.getTaskService().remove(userId, taskFirst);
    bootstrap.getTaskService().remove(userId, taskSecond);
    bootstrap.getProjectService().remove(userId, project);
    taskList = bootstrap.getTaskService().findAll(userId);
    Assert.assertEquals(0, taskList.size());
  }

}
